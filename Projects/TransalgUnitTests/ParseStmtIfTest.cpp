#include "TestMacros.h"

namespace
{

Transalg::StatementIfPtr ParseStatementIf(const std::string& text)
{
	using namespace Transalg;
	Parser parser;
	StatementBlockPtr program_block = parser.ParseProgramText(text);
	EXPECT_EQ(1, program_block->GetStatementsCount());
	StatementContainer::const_iterator stmt_it = program_block->begin();
	EXPECT_EQ(Transalg::stmtIf, (*stmt_it)->GetStatementType());
	StatementIfPtr ptr = boost::dynamic_pointer_cast<StatementIf>(*stmt_it);
	EXPECT_TRUE(ptr);
	return ptr;
}

} // local namespace

TEST(ParserTests, ParseStmtIfTest1)
{
	using namespace Transalg;
	const std::string text(
		"if(x & y)\n"
		"	shift(reg, 10);"
	);
	StatementIfPtr ptr = ParseStatementIf(text);
	// condition
	{
		ExpressionPtr cond = ptr->GetCondition();
		TestExpression(cond, 3);
		TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 1);
		TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, "&", 1);
	}
	// true-branch
	{
		StatementPtr stmt = ptr->GetTrueBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtExpression, stmt->GetStatementType());
		ExpressionPtr expr_ptr = boost::dynamic_pointer_cast<Expression>(stmt);
		EXPECT_TRUE(expr_ptr);
		TestExpression(expr_ptr, 3);
		TestExpressionOperand(expr_ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "reg", 2);
		TestExpressionOperand(expr_ptr->GetOperand(1), Transalg::OperandTypeConst, 0, "10", 2);
		TestExpressionOperand(expr_ptr->GetOperand(2), Transalg::OperandTypeFunction, 2, "shift", 2);
	}
	// false-branch
	{
		StatementPtr stmt = ptr->GetFalseBranch();
		EXPECT_FALSE(stmt);
	}
}

TEST(ParserTests, ParseStmtIfTest2)
{
	using namespace Transalg;
	const std::string text(
		"if(x > 0)\n"
		"{\n"
		"	x = y ^ 1;\n"
		"	shift(reg);\n"
		"}else{x = 0;}"
	);
	StatementIfPtr ptr = ParseStatementIf(text);
	// condition
	{
		ExpressionPtr cond = ptr->GetCondition();
		TestExpression(cond, 3);
		TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeConst, 0, "0", 1);
		TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, ">", 1);
	}
	// true-branch
	{
		StatementPtr stmt = ptr->GetTrueBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtBlock, stmt->GetStatementType());
		StatementBlockPtr block_ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		EXPECT_TRUE(block_ptr);
		EXPECT_EQ(2, block_ptr->GetStatementsCount());
		EXPECT_EQ(0, block_ptr->GetFunction());
		StatementPtr block_stmt1 = *(block_ptr->begin());
		EXPECT_TRUE(block_stmt1);
		EXPECT_EQ(Transalg::stmtExpression, block_stmt1->GetStatementType());
		StatementPtr block_stmt2 = *(block_ptr->begin() + 1);
		EXPECT_TRUE(block_stmt2);
		EXPECT_EQ(Transalg::stmtExpression, block_stmt1->GetStatementType());
	}
	// false-branch
	{
		StatementPtr stmt = ptr->GetFalseBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtBlock, stmt->GetStatementType());
		StatementBlockPtr block_ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		EXPECT_TRUE(block_ptr);
		EXPECT_EQ(1, block_ptr->GetStatementsCount());
		EXPECT_EQ(0, block_ptr->GetFunction());
		StatementPtr block_stmt = *(block_ptr->begin());
		EXPECT_TRUE(block_stmt);
		EXPECT_EQ(Transalg::stmtExpression, block_stmt->GetStatementType());
	}
}

TEST(ParserTests, ParseStmtIfTest3)
{
	using namespace Transalg;
	const std::string text(
		"if(x > 0)\n"
		"{\n"
		"	x = x - 1;\n"
		"}else if(x < 0)\n"
		"	x = x + 1;"
	);
	StatementIfPtr ptr = ParseStatementIf(text);
	// condition
	{
		ExpressionPtr cond = ptr->GetCondition();
		TestExpression(cond, 3);
		TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeConst, 0, "0", 1);
		TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, ">", 1);
	}
	// true-branch
	{
		StatementPtr stmt = ptr->GetTrueBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtBlock, stmt->GetStatementType());
		StatementBlockPtr block_ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		EXPECT_TRUE(block_ptr);
		EXPECT_EQ(1, block_ptr->GetStatementsCount());
		EXPECT_EQ(0, block_ptr->GetFunction());
	}
	// false-branch
	{
		StatementPtr stmt = ptr->GetFalseBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtIf, stmt->GetStatementType());
		StatementIfPtr if_ptr = boost::dynamic_pointer_cast<StatementIf>(stmt);
		EXPECT_TRUE(if_ptr);

		ExpressionPtr cond = if_ptr->GetCondition();
		EXPECT_TRUE(cond);
		TestExpression(cond, 3);
		TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 4);
		TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeConst, 0, "0", 4);
		TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, "<", 4);

		StatementPtr true_branch = if_ptr->GetTrueBranch();
		EXPECT_TRUE(true_branch);
		EXPECT_EQ(Transalg::stmtExpression, true_branch->GetStatementType());
		ExpressionPtr expr_ptr = boost::dynamic_pointer_cast<Expression>(true_branch);
		TestExpression(expr_ptr, 5);
		TestExpressionOperand(expr_ptr->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 5);
		TestExpressionOperand(expr_ptr->GetOperand(1), Transalg::OperandTypeVariable, 0, "x", 5);
		TestExpressionOperand(expr_ptr->GetOperand(2), Transalg::OperandTypeConst, 0, "1", 5);
		TestExpressionOperand(expr_ptr->GetOperand(3), Transalg::OperandTypeOperator, 2, "+", 5);
		TestExpressionOperand(expr_ptr->GetOperand(4), Transalg::OperandTypeOperator, 2, "=", 5);

		StatementPtr false_branch = if_ptr->GetFalseBranch();
		EXPECT_FALSE(false_branch);
	}
}

TEST(ParserTests, ParseStmtIfTest4)
{
	using namespace Transalg;
	const std::string text(
		"if(compare(x, y))\n"
		"{\n"
		"	if(x > 0)\n"
		"		shift_regA();\n"
		"	else if(x < 0)\n"
		"		shift_regB();\n"
		"}else{\n"
		"	z = z << 10;\n"
		"	if(y < 0)\n"
		"		shift_regC();\n"
		"}"
	);
	StatementIfPtr ptr = ParseStatementIf(text);
	// condition
	{
		ExpressionPtr cond = ptr->GetCondition();
		TestExpression(cond, 3);
		TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 1);
		TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeVariable, 0, "y", 1);
		TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeFunction, 2, "compare", 1);
	}
	// true-branch
	{
		StatementPtr stmt = ptr->GetTrueBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtBlock, stmt->GetStatementType());
		StatementBlockPtr block_ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		EXPECT_TRUE(block_ptr);
		EXPECT_EQ(1, block_ptr->GetStatementsCount());
		StatementIfPtr block_if_stmt = boost::dynamic_pointer_cast<StatementIf>(*(block_ptr->begin()));
		EXPECT_TRUE(block_if_stmt);
		// condition
		{
			ExpressionPtr cond = block_if_stmt->GetCondition();
			TestExpression(cond, 3);
			TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 3);
			TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeConst, 0, "0", 3);
			TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, ">", 3);
		}
		// true-branch
		{
			StatementPtr true_branch = block_if_stmt->GetTrueBranch();
			EXPECT_TRUE(true_branch);
			EXPECT_EQ(Transalg::stmtExpression, true_branch->GetStatementType());
			ExpressionPtr expr_ptr = boost::dynamic_pointer_cast<Expression>(true_branch);
			TestExpression(expr_ptr, 1);
			TestExpressionOperand(expr_ptr->GetOperand(0), Transalg::OperandTypeFunction, 0, "shift_regA", 4);
		}
		// false-branch
		{
			StatementPtr false_branch = block_if_stmt->GetFalseBranch();
			EXPECT_TRUE(false_branch);
			EXPECT_EQ(Transalg::stmtIf, false_branch->GetStatementType());
			StatementIfPtr if_ptr = boost::dynamic_pointer_cast<StatementIf>(false_branch);
			// condition
			{
				ExpressionPtr cond = if_ptr->GetCondition();
				TestExpression(cond, 3);
				TestExpressionOperand(cond->GetOperand(0), Transalg::OperandTypeVariable, 0, "x", 5);
				TestExpressionOperand(cond->GetOperand(1), Transalg::OperandTypeConst, 0, "0", 5);
				TestExpressionOperand(cond->GetOperand(2), Transalg::OperandTypeOperator, 2, "<", 5);
			}
			EXPECT_TRUE(if_ptr->GetTrueBranch());
			EXPECT_FALSE(if_ptr->GetFalseBranch());
		}
	}
	// false-branch
	{
		StatementPtr stmt = ptr->GetFalseBranch();
		EXPECT_TRUE(stmt);
		EXPECT_EQ(Transalg::stmtBlock, stmt->GetStatementType());
		StatementBlockPtr block_ptr = boost::dynamic_pointer_cast<StatementBlock>(stmt);
		EXPECT_EQ(2, block_ptr->GetStatementsCount());
		StatementPtr block_stmt1 = *(block_ptr->begin());
		EXPECT_TRUE(block_stmt1);
		EXPECT_EQ(Transalg::stmtExpression, block_stmt1->GetStatementType());
		StatementPtr block_stmt2 = *(block_ptr->begin() + 1);
		EXPECT_TRUE(block_stmt2);
		EXPECT_EQ(Transalg::stmtIf, block_stmt2->GetStatementType());
	}
}
