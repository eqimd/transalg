#include "stdafx.h"

TEST(OperationMultiply, EncodingTest)
{
	using namespace Transalg;

	//! Размер битовых векторов
	const std::size_t size = 4;
	EncodeFactory encode;

	//! Переменные кода: x1, x2, x3, x4
	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
	{
		vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));
	}

	//! Переменные кода: x5, x6, x7, x8
	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
	{
		vec2->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));
	}

	//! Кодируем целочисленное умножение
	BitVectorPtr result = SemanticLib::multiply(vec1, vec2, encode);

	const std::string expect_result(
		"[Vector]:\n"
		"\t[Bit]: x1 x5 &\n"
		"\t[Bit]: x9\n"
		"\t[Bit]: x16\n"
		"\t[Bit]: x23\n"
		"\t[Bit]: x26\n"
		"\t[Bit]: x28\n"
		"\t[Bit]: x29\n"
		"\t[Bit]: x4 x8 & x27 &\n"
	);
	const std::string actual_result(result->ToString());
	EXPECT_EQ(expect_result, actual_result);

	//! Ожидаемая пропозициональная кодировка целочисленного умноженая для данных векторов
	const std::string expect_mult_encode(
		"x9 = x2 x5 & x1 x6 & ^\n"
		"x10 = x2 x5 & x1 x6 & &\n"
		"x11 = x3 x5 & x2 x6 & & x3 x5 & x10 & x2 x6 & x10 & | |\n"
		"x12 = x3 x5 & x2 x6 & x10 ^ ^\n"
		"x13 = x4 x5 & x3 x6 & & x4 x5 & x11 & x3 x6 & x11 & | |\n"
		"x14 = x4 x5 & x3 x6 & x11 ^ ^\nx15 = x4 x6 & x13 ^\n"
		"x16 = x12 x1 x7 & ^\n"
		"x17 = x12 x1 x7 & &\n"
		"x18 = x14 x2 x7 & & x14 x17 & x2 x7 & x17 & | |\n"
		"x19 = x14 x2 x7 & x17 ^ ^\n"
		"x20 = x15 x3 x7 & & x15 x18 & x3 x7 & x18 & | |\n"
		"x21 = x15 x3 x7 & x18 ^ ^\n"
		"x22 = x4 x7 & x20 ^ x4 x6 & x13 & ^\n"
		"x23 = x19 x1 x8 & ^\n"
		"x24 = x19 x1 x8 & &\n"
		"x25 = x21 x2 x8 & & x21 x24 & x2 x8 & x24 & | |\n"
		"x26 = x21 x2 x8 & x24 ^ ^\n"
		"x27 = x22 x3 x8 & & x22 x25 & x3 x8 & x25 & | |\n"
		"x28 = x22 x3 x8 & x25 ^ ^\n"
		"x29 = x4 x8 & x27 ^ x4 x7 & x20 & ^\n"
	);

	//! Сравниваем реальную кодировку с ожидаемой
	std::stringstream ss;
	LogicEquationSerializer les(ss);
	les.WriteOut(encode.tseitin_vars);
	EXPECT_EQ(expect_mult_encode, ss.str());
}

TEST(OperationMultiply, EncodingOneBitTest)
{
	using namespace Transalg;
	{
		EncodeFactory encode;

		BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
		vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

		BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
		vec2->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));
		
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, encode);
		EXPECT_EQ(0, encode.tseitin_vars.size());

		const std::string expect_mult_encode("[Vector]:\n\t[Bit]: x1 x2 &\n");
		const std::string actual_mult_encode(result->ToString());
		EXPECT_EQ(expect_mult_encode, actual_mult_encode);		
	}
}

//! Сложение вектора из переменных кода и константного вектора
TEST(OperationMultiply, EncodeWithRealDataTest)
{
	using namespace Transalg;
	
	EncodeFactory encode;
	const std::size_t size(3);

	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
		vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::False());
	vec2->PushBack(EncodeFactory::True());

	BitVectorPtr result = SemanticLib::multiply(vec1, vec2, encode);

	const std::string expect_result(
		"[Vector]:\n"
		"\t[Bit]: x1\n"
		"\t[Bit]: x4\n"
		"\t[Bit]: x11\n"
		"\t[Bit]: x14\n"
		"\t[Bit]: x16\n"
		"\t[Bit]: x17\n"
		"\t[Bit]: x3 x15 &\n"
	);
	const std::string actual_result(result->ToString());
	EXPECT_EQ(expect_result, actual_result);

	const std::string expect_encode(
		"x4 = x1 x2 ^\n"
		"x5 = x1 x2 &\n"
		"x6 = x2 x5 &\n"
		"x7 = x2 x5 ^\n"
		"x8 = x1 x6 &\n"
		"x9 = x1 x6 ^\n"
		"x10 = x2 x8 ^\n"
		"x11 = x7 x3 ^\n"
		"x12 = x7 x3 &\n"
		"x13 = x9 x3 & x9 x12 & x3 x12 & | |\n"
		"x14 = x9 x3 x12 ^ ^\n"
		"x15 = x10 x13 &\n"
		"x16 = x10 x13 ^\n"
		"x17 = x3 x15 ^ x2 x8 & ^\n"
	);

	std::stringstream ss;
	LogicEquationSerializer les(ss);
	les.WriteOut(encode.tseitin_vars);
	EXPECT_EQ(expect_encode, ss.str());
}

TEST(OperationMultiply, RealDataTest)
{
	using namespace Transalg;
	//! Кодируем целочисленное умножение
	EncodeFactory encode;
	//! 4 * 5 = 20
	{
		BitVectorPtr vec1 = boost::make_shared<BitVector>("0x5");
		BitVectorPtr vec2 = boost::make_shared<BitVector>("0x4");
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x14");
		BitVectorPtr actual_result = SemanticLib::multiply(vec1, vec2, encode);
		EXPECT_EQ(*expect_result, *actual_result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}
	//! 3 * 15 = 45
	{
		BitVectorPtr vec1 = boost::make_shared<BitVector>("0x3");
		BitVectorPtr vec2 = boost::make_shared<BitVector>("0xf");
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x2d");
		BitVectorPtr actual_result = SemanticLib::multiply(vec1, vec2, encode);
		EXPECT_EQ(*expect_result, *actual_result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}
	//! 1 * 20 = 20
	{
		BitVectorPtr vec1 = boost::make_shared<BitVector>("0x1");
		BitVectorPtr vec2 = boost::make_shared<BitVector>("0x14");
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x14");
		BitVectorPtr actual_result = SemanticLib::multiply(vec1, vec2, encode);
		EXPECT_EQ(*expect_result, *actual_result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}
	//! 1 * 0 = 0
	{
		BitVectorPtr vec1 = boost::make_shared<BitVector>("0x1");
		BitVectorPtr vec2 = boost::make_shared<BitVector>("0x0");
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x0");
		BitVectorPtr actual_result = SemanticLib::multiply(vec1, vec2, encode);
		EXPECT_EQ(*expect_result, *actual_result);
	}
}

TEST(OperationMultiplyMod, EncodingTest)
{
	using namespace Transalg;
	//! Размер битовых векторов
	const std::size_t size = 4;
	EncodeFactory encode;

	//! Переменные кода: x1, x2, x3, x4
	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
		vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

	//! Переменные кода: x5, x6, x7, x8
	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
		vec2->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

	//! Умножаем два кодированных 4-битных числа по модулю 2^4 (общий случай)
	{
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 4, encode);

		const std::string expect_result(
			"[Vector]:\n"
			"\t[Bit]: x1 x5 &\n"
			"\t[Bit]: x10\n"
			"\t[Bit]: x15\n"
			"\t[Bit]: x17\n"
		);
		const std::string actual_result(result->ToString());
		EXPECT_EQ(expect_result, actual_result);

		const std::string expect_encode(
			"x9 = x2 x5 & x1 x6 & &\n"
			"x10 = x2 x5 & x1 x6 & ^\n"
			"x11 = x3 x5 & x2 x6 & & x3 x5 & x9 & x2 x6 & x9 & | |\n"
			"x12 = x3 x5 & x2 x6 & x9 ^ ^\n"
			"x13 = x4 x5 & x3 x6 & x11 ^ ^\n"
			"x14 = x12 x1 x7 & &\n"
			"x15 = x12 x1 x7 & ^\n"
			"x16 = x13 x2 x7 & x14 ^ ^\n"
			"x17 = x16 x1 x8 & ^\n"
		);
		std::stringstream ss;
		LogicEquationSerializer les(ss);
		les.WriteOut(encode.tseitin_vars);
		EXPECT_EQ(expect_encode, ss.str());
	}

	//! Умножаем два кодированных 4-битных числа по модулю 2^0 (граничный случай)
	{
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 1, encode);

		//! Нумерация переменных продолжается с посленего использованного номера
		const std::string expect_result("[Vector]:\n\t[Bit]: x18\n");
		const std::string actual_result(result->ToString());
		EXPECT_EQ(expect_result, actual_result);

		const std::string expect_encode("x18 = x1 x5 &");
		std::stringstream ss;
		LogicEquationSerializer les(ss);
		les.WriteOut(encode.tseitin_vars.back());
		EXPECT_EQ(expect_encode, ss.str());
	}
}

TEST(OperationMultiplyMod, EncodeWithRealDataTest)
{
	using namespace Transalg;
	//! Размер битовых векторов
	const std::size_t size = 4;
	EncodeFactory encode;

	//! Переменные кода: x1, x2, x3, x4
	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	for(unsigned i = 0; i < size; ++i)
		vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::False());
	vec2->PushBack(EncodeFactory::True());

	//! Умножаем кодированное 4-битное число на 4-битную константу по модулю 2^4 (частный случай)
	{
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 4, encode);

		const std::string expect_result(
			"[Vector]:\n"
			"\t[Bit]: x1\n"
			"\t[Bit]: x6\n"
			"\t[Bit]: x8\n"
			"\t[Bit]: x10\n"
		);
		const std::string actual_result(result->ToString());
		EXPECT_EQ(expect_result, actual_result);

		const std::string expect_encode(
			"x5 = x2 x1 &\n"
			"x6 = x2 x1 ^\n"
			"x7 = x3 x2 & x3 x5 & x2 x5 & | |\n"
			"x8 = x3 x2 x5 ^ ^\n"
			"x9 = x4 x3 x7 ^ ^\n"
			"x10 = x9 x1 ^\n"
		);
		std::stringstream ss;
		LogicEquationSerializer les(ss);
		les.WriteOut(encode.tseitin_vars);
		EXPECT_EQ(expect_encode, ss.str());
	}

	//! Умножаем кодированное 4-битное число на 4-битную константу 
	//! по модулю 2^10 (модуль значительно превосходит возможный результат)
	{
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 10, encode);

		const std::string expect_result(
			"[Vector]:\n"
			"\t[Bit]: x1\n"
			"\t[Bit]: x6\n"
			"\t[Bit]: x8\n"
			"\t[Bit]: x10\n"
			"\t[Bit]: x16\n"
			"\t[Bit]: x18\n"
			"\t[Bit]: x19\n"
			"\t[Bit]: x4 x17 &\n"
		);
		const std::string actual_result(result->ToString());
		EXPECT_EQ(expect_result, actual_result);

		const std::string expect_encode(
			"x5 = x2 x1 &\n"
			"x6 = x2 x1 ^\n"
			"x7 = x3 x2 & x3 x5 & x2 x5 & | |\n"
			"x8 = x3 x2 x5 ^ ^\n"
			"x9 = x4 x3 x7 ^ ^\n"
			"x10 = x9 x1 ^\n"
			"x11 = x4 x3 & x4 x7 & x3 x7 & | |\n"
			"x12 = x4 x11 ^\n"
			"x13 = x4 x11 &\n"
			"x14 = x9 x1 &\n"
			"x15 = x12 x2 & x12 x14 & x2 x14 & | |\n"
			"x16 = x12 x2 x14 ^ ^\n"
			"x17 = x13 x3 & x13 x15 & x3 x15 & | |\n"
			"x18 = x13 x3 x15 ^ ^\n"
			"x19 = x4 x17 ^\n"
		);
		std::stringstream ss;
		LogicEquationSerializer les(ss);
		les.WriteOut(encode.tseitin_vars);
		EXPECT_EQ(expect_encode, ss.str());
	}
}

TEST(OperationMultiplyMod, EncodingOneBitTest)
{
	using namespace Transalg;
	EncodeFactory encode;

	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	vec1->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));

	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	vec2->PushBack(encode.CreateVarFormula(Transalg::LogicVarInput));
	
	BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 2, encode);

	const std::string expect_result("[Vector]:\n\t[Bit]: x1 x2 &\n");
	const std::string actual_result(result->ToString());
	EXPECT_EQ(expect_result, actual_result);
	EXPECT_EQ(0, encode.tseitin_vars.size());
}

TEST(OperationMultiplyMod, RealDataTest)
{
	using namespace Transalg;

	//! 11 = 0xB
	BitVectorPtr vec1 = boost::make_shared<BitVector>(0);
	vec1->PushBack(EncodeFactory::True());
	vec1->PushBack(EncodeFactory::True());
	vec1->PushBack(EncodeFactory::False());
	vec1->PushBack(EncodeFactory::True());

	//! 25 = 0x19
	BitVectorPtr vec2 = boost::make_shared<BitVector>(0);
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::False());
	vec2->PushBack(EncodeFactory::False());
	vec2->PushBack(EncodeFactory::True());
	vec2->PushBack(EncodeFactory::True());

	//! 11 * 25 = 275 = 0x113 (по модулю 2^10) = 0x113
	{
		EncodeFactory encode;
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 10, encode);
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x113");
		EXPECT_EQ(*expect_result, *result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}

	//! 11 * 25 = 275 = 0x113 (по модулю 2^5) = 0x13
	{
		EncodeFactory encode;
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 5, encode);
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x13");
		EXPECT_EQ(*expect_result, *result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}

	//! 11 * 25 = 275 = 0x113 (по модулю 2^2) = 0x3
	{
		EncodeFactory encode;
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 2, encode);
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x3");
		EXPECT_EQ(*expect_result, *result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}

	//! 11 * 25 = 275 = 0x113 (по модулю 2^1) = 0x1
	{
		EncodeFactory encode;
		BitVectorPtr result = SemanticLib::multiply(vec1, vec2, 1, encode);
		BitVectorPtr expect_result = boost::make_shared<BitVector>("0x1");
		EXPECT_EQ(*expect_result, *result);
		EXPECT_EQ(0, encode.tseitin_vars.size());
	}
}
