#include "stdafx.h"
#include "../EncodeFactory.h"

namespace Transalg
{

const LogicOperand EncodeFactory::true_operand_ = LogicOperand(LogicValueTrue);
const LogicOperand EncodeFactory::false_operand_ = LogicOperand(LogicValueFalse);

const LogicOperand EncodeFactory::not_operand_ = LogicOperand(LogicFunctionNot);
const LogicOperand EncodeFactory::and_operand_ = LogicOperand(LogicFunctionAnd);
const LogicOperand EncodeFactory::or_operand_ = LogicOperand(LogicFunctionOr);
const LogicOperand EncodeFactory::xor_operand_ = LogicOperand(LogicFunctionXor);
const LogicOperand EncodeFactory::equiv_operand_ = LogicOperand(LogicFunctionEquiv);

const LogicFormulaPtr EncodeFactory::true_ = boost::make_shared<LogicFormula>(LogicOperand(LogicValueTrue));
const LogicFormulaPtr EncodeFactory::false_ = boost::make_shared<LogicFormula>(LogicOperand(LogicValueFalse));
const std::string EncodeFactory::var_id_("x");

const size_t EncodeFactory::max_var_set_size_ = 10;

EncodeFactory::EncodeFactory()
	: encode_var_count_(0)
	, removed_var_count_(0)
{}

const LogicOperand& EncodeFactory::TrueOperand()
{
	return true_operand_;
}

const LogicOperand& EncodeFactory::FalseOperand()
{
	return false_operand_;
}

LogicFormulaPtr EncodeFactory::True()
{
	return true_;
}

LogicFormulaPtr EncodeFactory::False()
{
	return false_;
}

unsigned EncodeFactory::GetEncodeVarCount() const
{
	return encode_var_count_;
}

void EncodeFactory::ClearEncode()
{
	input_vars.clear();
	tseitin_vars.clear();
	output_vars.clear();
	constraints.clear();
	encode_var_count_ = 0;
}

void EncodeFactory::RenumberingVariables()
{
	encode_var_count_ = 0;
	size_t i;
	for(i = 0; i < input_vars.size(); ++i)
		input_vars[i]->var_id = ++encode_var_count_;

	for(i = 0; i < tseitin_vars.size(); ++i)
		tseitin_vars[i]->var_id = ++encode_var_count_;

	for(i = 0; i < output_vars.size(); ++i)
		output_vars[i]->var_id = ++encode_var_count_;
}

LogicVarPtr EncodeFactory::CreateVar(LogicFormulaPtr value)
{
	LogicVarPtr var;
	if(value)
	{
		//! Проверим словарь термов: если там уже есть такая формула, то для нее уже создана переменная кода
		TermMap::const_iterator term_it = terms_.find(*value);
		if(term_it != terms_.end())
		{
			var = term_it->second;
			assert(var);
		}
	}

	//! Если подходящую переменную не нашли через словарь термов, то создаем новую переменную
	if(!var)
	{
		var = boost::make_shared<LogicVar>(++encode_var_count_, LogicVarTseitin, value);
		//! Если новая переменная опреляется через формулу, то занесем эту формулу в словарь термов
		if(value)
		{
			terms_[*value] = var;
		}
		tseitin_vars.push_back(var);
	}
	
	return var;
}

LogicVarPtr EncodeFactory::CreateVar(LogicVarType type, LogicFormulaPtr value)
{
	if(type == Transalg::LogicVarInput)
	{
		LogicVarPtr var = boost::make_shared<LogicVar>(++encode_var_count_, type, value);
		input_vars.push_back(var);
		return var;
	}
	else if(type == Transalg::LogicVarOutput)
	{
		LogicVarPtr var = boost::make_shared<LogicVar>(++encode_var_count_, type, value);
		output_vars.push_back(var);
		return var;
	}
	else
	{
		return CreateVar(value);
	}
	assert(0);
	return LogicVarPtr();
}

LogicFormulaPtr EncodeFactory::CreateVarFormula(LogicFormulaPtr var_value)
{
	LogicVarPtr var = CreateVar(var_value);
	return boost::make_shared<LogicFormula>(var);
}

LogicFormulaPtr EncodeFactory::CreateVarFormula(LogicVarType var_type, LogicFormulaPtr var_value)
{
	LogicVarPtr var = CreateVar(var_type, var_value);
	return boost::make_shared<LogicFormula>(var);
}

LogicFormulaPtr EncodeFactory::CreateVarIfFunction(LogicFormulaPtr formula)
{
	if(formula->IsFunction())
	{
		return boost::make_shared<LogicFormula>(CreateVar(formula));
	}
	return formula;
}

LogicFormulaPtr EncodeFactory::Not(LogicFormulaPtr formula)
{
	if(!formula) 
	{
		LOG_WARNING("EncodeFactory::Not: formula - null pointer!");
		return LogicFormulaPtr();
	}
	if(formula->IsConst())
	{
		const LogicOperand operand(!formula->back().value);
		return boost::make_shared<LogicFormula>(operand);
	}
	//! Копируем исходную формулу и добавляем к ней отрицание
	LogicFormulaPtr result = boost::make_shared<LogicFormula>();
	result->insert(result->end(), formula->begin(), formula->end());
	result->push_back(not_operand_);
	return result;
}

LogicFormulaPtr EncodeFactory::And(LogicFormulaPtr x, LogicFormulaPtr y)
{
	if(!x && !y)
	{
		LOG_WARNING("EncodeFactory::And: x - null pointer, y - null pointer!");
		return LogicFormulaPtr();
	}
	if(!x) return y;
	if(!y) return x;

	const bool x_is_const(x->IsConst());
	const bool y_is_const(y->IsConst());

	if(x_is_const || y_is_const)
	{
		const LogicValueType x_value(x->back().value);
		const LogicValueType y_value(y->back().value);

		if(x_is_const && y_is_const)
			return ((x_value && y_value).True() ? True() : False());
		if(x_is_const)
			return (x_value.True() ? y : False());
		if(y_is_const)
			return (y_value.True() ? x : False());
	}
	LogicFormulaPtr result = boost::make_shared<LogicFormula>();
	result->insert(result->end(), x->begin(), x->end());
	result->insert(result->end(), y->begin(), y->end());
	result->push_back(and_operand_);
	return result;
}

LogicFormulaPtr EncodeFactory::Or(LogicFormulaPtr x, LogicFormulaPtr y)
{
	if(!x && !y)
	{
		LOG_WARNING("EncodeFactory::Or: x - null pointer, y - null pointer!");
		return LogicFormulaPtr();
	}
	if(!x) return y;
	if(!y) return x;

	const bool x_is_const(x->IsConst());
	const bool y_is_const(y->IsConst());

	if(x_is_const || y_is_const)
	{
		const LogicValueType x_value(x->back().value);
		const LogicValueType y_value(y->back().value);

		if(x_is_const && y_is_const)
			return ((x_value || y_value).True() ? True() : False());
		if(x_is_const)
			return (x_value.True() ? True() : y);
		if(y_is_const)
			return (y_value.True() ? True() : x);
	}
	LogicFormulaPtr result = boost::make_shared<LogicFormula>();
	result->insert(result->end(), x->begin(), x->end());
	result->insert(result->end(), y->begin(), y->end());
	result->push_back(or_operand_);
	return result;
}

LogicFormulaPtr EncodeFactory::Xor(LogicFormulaPtr x, LogicFormulaPtr y)
{
	if(!x && !y)
	{
		LOG_WARNING("EncodeFactory::Xor: x - null pointer, y - null pointer!");
		return LogicFormulaPtr();
	}
	if(!x) return y;
	if(!y) return x;

	const bool x_is_const(x->IsConst());
	const bool y_is_const(y->IsConst());

	if(x_is_const || y_is_const)
	{
		const LogicValueType x_value(x->back().value);
		const LogicValueType y_value(y->back().value);

		if(x_is_const && y_is_const) 
			return ((x_value ^ y_value).True() ? True() : False());
		if(x_is_const)
			return (x_value.True() ? Not(y) : y);
		if(y_is_const)
			return (y_value.True() ? Not(x) : x);
	}
	LogicFormulaPtr result = boost::make_shared<LogicFormula>();
	result->insert(result->end(), x->begin(), x->end());
	result->insert(result->end(), y->begin(), y->end());
	result->push_back(xor_operand_);
	return result;
}

LogicFormulaPtr EncodeFactory::Eq(LogicFormulaPtr x, LogicFormulaPtr y)
{
	if(!x && !y)
	{
		LOG_WARNING("EncodeFactory::Eq: x - null pointer, y - null pointer!");
		return LogicFormulaPtr();
	}
	if(!x) return y;
	if(!y) return x;

	const bool x_is_const(x->IsConst());
	const bool y_is_const(y->IsConst());
	
	if(x_is_const || y_is_const)
	{
		const LogicValueType x_value(x->back().value);
		const LogicValueType y_value(y->back().value);

		if(x_is_const && y_is_const)
			return (x_value == y_value ? True() : False());
		if(x_is_const)
			return (x_value.True() ? y : Not(y));
		if(y_is_const)
			return (y_value.True() ? x : Not(x));
	}
	LogicFormulaPtr result = boost::make_shared<LogicFormula>();
	result->insert(result->end(), x->begin(), x->end());
	result->insert(result->end(), y->begin(), y->end());
	result->push_back(equiv_operand_);
	return result;
}

LogicFormulaPtr EncodeFactory::Majority(LogicFormulaPtr x, LogicFormulaPtr y, LogicFormulaPtr z)
{
	return Or(And(x,y),Or(And(x,z),And(y,z)));
}

} // namespace Transalg
