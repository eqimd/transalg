#include "stdafx.h"
#include "EspressoUtils.h"

//! Espresso - boolean minimization algorithm
extern "C"
{
#include <EspressoLib/espresso.h>
}

namespace Transalg
{

namespace
{

char** new_table(unsigned xsize, unsigned ysize)
{
	char** table = (char**)calloc(sizeof(char*), xsize);
	assert(table);
	for (unsigned i = 0; i < xsize; ++i)
	{
		table[i] = (char*)calloc(sizeof(char), ysize);
		assert(table[i]);
	}
	return table;
}

void free_table(char** table, unsigned ix)
{
	if(table == 0) return;
	for(unsigned i = 0; i < ix; ++i)
		if(table[i]) free(table[i]);
	free(table);
}

/**
* @brief Записать таблицу истинности в структуру PLA_t.
* @param table [in] - таблица истинности.
* @return Возвращает указатель на заполняемую структуру PLA_t.
*/
pPLA GetPla(const Table& table)
{
	pPLA PLA(NULL);
	PLA = NIL(PLA_t);
	PLA = new_PLA();
    PLA->pla_type = FD_type;

	//! Уставновка входных переменных
	assert(cube.fullset == NULL);
	cube.num_binary_vars = table.cols;
	cube.num_vars = cube.num_binary_vars + 1;
	cube.part_size = ALLOC(int, cube.num_vars);

	//! Установка выходных переменных
	assert(cube.fullset == NULL);
	cube.part_size[cube.num_vars - 1] = 1;
	cube_setup();

	if(cube.fullset == NULL)
	{
		std::cerr << "cube.fullset == NULL" << std::endl;
		exit(1);
	}

	if (PLA->F == NULL)
	{
		PLA->F = new_cover(10);
		PLA->D = new_cover(10);
		PLA->R = new_cover(10);
	}

	//! Чтение куба
	for(unsigned row(0); row < table.rows; ++row)
	{
		pcube cf = cube.temp[0], cr = cube.temp[1], cd = cube.temp[2];
		set_clear(cf, cube.size);
		for(int var(0); var < cube.num_binary_vars; ++var)
		{
			if(table.data[row][var])
				set_insert(cf, var*2 + 1);
			else
				set_insert(cf, var*2);
		}

		set_copy(cr, cf);
		set_copy(cd, cf);

		set_insert(cf, cube.first_part[cube.num_binary_vars]);
		PLA->F = sf_addset(PLA->F, cf);
	}

	////////////////////////////////////////////////////////////////////////

	int needs_dcset = TRUE, needs_offset = TRUE;

    // This hack merges the next-state field with the outputs
    for(int i = 0; i < cube.num_vars; i++)
	{
		cube.part_size[i] = ABS(cube.part_size[i]);
    }

    // Decide how to break PLA into ON-set, OFF-set and DC-set
    if (pos || PLA->phase != NULL || PLA->symbolic_output != NIL(symbolic_t))
	{
		needs_offset = TRUE;
    }
    if (needs_offset && (PLA->pla_type==F_type || PLA->pla_type==FD_type))
	{
		free_cover(PLA->R);
		PLA->R = complement(cube2list(PLA->F, PLA->D));
    } 
	else if (needs_dcset && PLA->pla_type == FR_type)
	{
		pcover X;
		free_cover(PLA->D);
		// hack, why not?
		X = d1merge(sf_join(PLA->F, PLA->R), cube.num_vars - 1);
		PLA->D = complement(cube1list(X));
		free_cover(X);
    } 
	else if (PLA->pla_type == R_type || PLA->pla_type == DR_type)
	{
		free_cover(PLA->F);
		PLA->F = complement(cube2list(PLA->D, PLA->R));
    }

    // Check for phase rearrangement of the functions
    if (pos)
	{
		pcover onset = PLA->F;
		PLA->F = PLA->R;
		PLA->R = onset;
		PLA->phase = new_cube();
		set_diff(PLA->phase, cube.fullset, cube.var_mask[cube.num_vars-1]);
    }
	else if (PLA->phase != NULL) 
	{
		set_phase(PLA);
    }

    // Setup minimization for two-bit decoders
    if (PLA->pair != (ppair) NULL)
	{
		set_pair(PLA);
    }

	return PLA;
}

/**
* @brief Разбор результатов минимизации, сохранение их в таблице.
* @param PLA [in] - структура PLA_t, содержащая результаты минимизации.
* @param table [out] - таблица, в которую сохраняются результаты.
*/
void ParsePla(pPLA PLA, Table& table)
{
	table.alloc_rows = table.rows = PLA->R->count;
	table.alloc_cols = table.cols = cube.num_binary_vars;
	table.data = new_table(table.rows, table.cols);

	register pcube last, p;
	register int var, row;

	row = 0;
	foreach_set(PLA->R, last, p)
	{
		for(var = 0; var < cube.num_binary_vars; ++var)
		{
			switch(GETINPUT(p, var))
			{
			case 1: // '0'
				table.data[row][var] = '0';
				break;
			case 2: // '1'
				table.data[row][var] = '1';
				break;
			case 3: // '-'
				table.data[row][var] = '-';
				break;
			default:
				table.data[row][var] = '?';
			}
		}
		++row;
	}
}

}

Table::Table(size_t x, size_t y)
	: rows(x)
	, cols(y)
	, alloc_rows(x)
	, alloc_cols(y)
{
	if(alloc_rows > 0)
	{
		data = new_table(alloc_rows, alloc_cols);
	}
}

Table::~Table()
{
	if(alloc_rows > 0)
	{
		free_table(data, alloc_rows);
	}
}

void Table::ResetRows(size_t used_rows)
{
	assert(used_rows <= alloc_rows);
	rows = used_rows;
}

void Table::ResetCols(size_t used_cols)
{
	assert(used_cols <= alloc_cols);
	cols = used_cols;
}

/**
* @brief Минимизация булевой формулы при помощи алгоритма Espresso.
* @param table_in [in] - строки таблицы истинности, на которых минимизируемая формула принимает значение истина.
* @param table_out [out] - таблица, описывающая результат минимизации формулы.
* @return Возвращает код ошибки (0 если все хорошо).
*/
int Minimize(const Table& table_in, Table& table_out)
{
    pcover Fold;
    cost_t cost;

	pPLA PLA = GetPla(table_in);

	Fold = sf_save(PLA->F);
	PLA->F = espresso(PLA->F, PLA->D, PLA->R);

	int error(0);
	EXECUTE(error = verify(PLA->F, Fold, PLA->D), VERIFY_TIME, PLA->F, cost);
	if (error) 
	{
	    print_solution = FALSE;
	    PLA->F = Fold;
	    (void) check_consistency(PLA);
	    std::cerr << "Minimize: cover verification failed!" << std::endl;
	    return error;
	} 
	else 
	{
	    free_cover(Fold);
	}

	ParsePla(PLA, table_out);

	// cleanup all used memory
    free_PLA(PLA);
    FREE(cube.part_size);
    setdown_cube();             // free the cube/cdata structure data
    sf_cleanup();               // free unused set structures
    sm_cleanup();               // sparse matrix cleanup
	return error;
}

/**
* @brief Вывод результата в поток.
* @param out [in] - ссылка на поток, куда выводим результаты.
* @param table [in] - таблица истинности.
*/
void PrintTable(std::ostream& out, const Table& table)
{
	for(size_t i(0); i < table.rows; ++i)
	{
		for(size_t j(0); j < table.cols; ++j)
		{
			switch(table.data[i][j])
			{
			case '0': out << 0; break;
			case '1': out << 1; break;
			default:
				out << "-";
			}
		}
		out << " 0\n";
	}
}

/**
* @brief Записать таблицу в PLA-файл.
* Таблица может потом использоваться в качестве входных данных Espresso.
* Только для таблиц булевых функций.
* @param filename [in] Имя файла, в который записываем таблицу.
* @param table [in] Строки таблицы истинности, на которых минимизируемая формула принимает значение истина.
*/
void WritePla(const char* filename, const Table& table)
{
	std::ofstream out(filename, std::ios::out);
	if(!out.is_open())
	{
		std::cerr << "WritePLA: can't open file '" << filename << "'!" << std::endl;
		return;
	}
	// Хедер PLA-файла
	out << ".i " << table.cols << std::endl;
	out << ".o 1" << std::endl;
	out << ".ibl";
	for(size_t i(0); i < table.cols; ++i)
	{
		out << " x" << i;
	}
	out << std::endl;
	out << ".ob y" << std::endl;
	out << ".p " << table.rows << std::endl;
	// Таблица истинности
	for(size_t i(0); i < table.rows; ++i)
	{
		for(size_t j(0); j < table.cols; ++j)
		{
			out << (table.data[i][j] == 1 ? '1' : '0');
		}
		out << " 1" << std::endl;
	}
	out << ".e";
}

void WriteCnfDimacs(std::ostream& out, const Table& table, const std::vector<unsigned>& var_numbers, 
					size_t& disjuncts, size_t& literals)
{
	for(size_t i(0); i < table.rows; ++i)
	{
		for(size_t j(0); j < table.cols; ++j)
		{
			switch(table.data[i][j])
			{
			case '0': 
				out << var_numbers[j] << " "; 
				++literals;
				break;
			case '1': 
				out << "-" << var_numbers[j] << " "; 
				++literals;
				break;
			}
		}
		out << "0\n";
		++disjuncts;
	}
}

} // namespace Transalg
