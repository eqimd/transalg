#include "stdafx.h"
#include "../VarLayers.h"

namespace Transalg
{

VarLayers::VarLayers(const LogicVarContainer& vars)
{	
	var_count_ = vars.size();
	std::map<VarId, unsigned> var_layer_map;

	layers_.push_back(boost::make_shared<VarIdContainer>());
	for(unsigned i(0); i < var_count_; ++i)
	{
		LogicVarPtr var = vars[i];
		const VarId var_id = var->var_id;
		if(var_layer_map.find(var_id) != var_layer_map.end())
		{
			throw std::runtime_error("VarLayers::VarLayers: variable is already added to the layers.");
		}

		LogicFormulaPtr value = var->var_value;
		
		// Переменная независимая - добавляем в первый слой (переменные входа)
		if(!value)
		{
			layers_[0]->push_back(var_id);
			var_layer_map[var_id] = 0;
			continue;
		}

		// Не зависит от переменных входа - игнорируем
		if(value->IsConst())
		{
			continue;
		}

		// Данная переменная эквивалентная другой переменной - добавляем ее в тот же слой
		if(value->IsVariable())
		{
			LogicVarPtr ptr = boost::static_pointer_cast<LogicVar>(value->front().var);
			const VarId in_var_id = ptr->var_id;
			std::map<VarId, unsigned>::const_iterator var_it = var_layer_map.find(in_var_id);
			if(var_it == var_layer_map.end())
			{
				throw std::runtime_error("VarLayers::VarLayers: variable is not added to the layers.");
			}
			const unsigned layer_index(var_it->second);
			layers_[layer_index]->push_back(var_id);
			var_layer_map[var_id] = layer_index;
		}

		// Переменная определяется через функцию от других переменных
		if(value->IsFunction())
		{
			LogicFormulaPtr ptr = boost::static_pointer_cast<LogicFormula>(value);
			LogicVarSet input_vars;
			ptr->GetVariableSet(input_vars);
			unsigned min_layer_index(std::numeric_limits<unsigned>::max());
			unsigned max_layer_index(0);

			for(LogicVarSet::const_iterator it = input_vars.begin(); it != input_vars.end(); ++it)
			{
				const VarId in_var_id = (*it)->var_id;
				std::map<VarId, unsigned>::const_iterator var_it = var_layer_map.find(in_var_id);
				if(var_it == var_layer_map.end())
				{
					throw std::runtime_error("VarLayers::VarLayers: variable is not added to the layers.");
				}
				if(var_it->second > max_layer_index)
				{
					max_layer_index = var_it->second;
				}
				if(var_it->second < min_layer_index)
				{
					min_layer_index = var_it->second;
				}
			}

			// Добавляем переменную в слой и если необходимо добавляем новый слой
			const unsigned layer_index = max_layer_index + 1;
			while(layer_index >= layers_.size())
			{
				layers_.push_back(boost::make_shared<VarIdContainer>());
			}
			layers_[layer_index]->push_back(var_id);
			var_layer_map[var_id] = layer_index;

			//! Дистанция между слоями
			if(layers_distance_.size() < layers_.size())
			{
				layers_distance_.resize(layers_.size(), 0);
			}
			layers_distance_[layer_index] = layer_index - min_layer_index;
		}
	}

	// Сортируем номера переменные внутри каждого слоя по возрастанию
	for(unsigned i(0); i < layers_.size(); ++i)
	{
		VarIdContainer& layer = *(layers_[i]);
		std::sort(layer.begin(), layer.end());
	}
}

std::size_t VarLayers::GetLayerCount() const
{
	return layers_.size();
}

std::size_t VarLayers::GetVarCount() const
{
	return var_count_;
}

const VarIdContainer& VarLayers::GetLayer(unsigned index) const
{
	VarIdContainerPtr vars = layers_.at(index);
	if(!vars)
		throw std::runtime_error("VarLayers::GetLayer: vars - null pointer.");
	return *vars;
}

void VarLayers::WriteOutLayers(std::ostream& out) const
{
	for(unsigned i(0); i < layers_.size(); ++i)
	{
		const VarIdContainer& layer = *(layers_[i]);
		out << "[" << i << "]";
		out << "[" << layer.size() << "]";
		// В соотвествии с решателем нумеруем переменные с нуля
		for(unsigned j(0); j < layer.size(); ++j)
			out << " " << (layer[j] - 1);
		out << std::endl << std::endl;
	}
}

void VarLayers::WriteLayersDimacs(std::ostream& out) const
{
	out << "c begin_layers_list" << std::endl;
	for(unsigned i(0); i < layers_.size(); ++i)
	{
		const VarIdContainer& layer = *(layers_[i]);
		out << "c";
		// Оставляем нумерацию переменных, начиная с единицы
		for(unsigned j(0); j < layer.size(); ++j)
		{
			out << " " << layer[j];
		}
		out << " 0" << std::endl;
	}
	out << "c end_layers_list" << std::endl;
}

void VarLayers::WriteOutVarAssigns(std::ostream& out) const
{
	for(Layers::const_reverse_iterator it = layers_.rbegin();
		it != layers_.rend();
		++it)
	{
		const VarIdContainer& layer = **it;
		for(VarIdContainer::const_reverse_iterator var_it = layer.rbegin();
			var_it != layer.rend();
			++var_it)
		{
			// В соотвествии с решателем нумеруем переменные с нуля
			out << (*var_it - 1) << std::endl;
		}
	}
}

void VarLayers::WriteOutLayersDistance(std::ostream& out) const
{
	for(unsigned i(0); i < layers_distance_.size(); ++i)
	{
		out << "[" << i << "]: " << layers_distance_[i] << std::endl;
	}
}

} // namespace Transalg
