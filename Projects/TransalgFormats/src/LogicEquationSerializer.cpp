#include "stdafx.h"
#include "../LogicEquationSerializer.h"

namespace Transalg
{

LogicEquationSerializer::LogicEquationSerializer(std::ostream& out)
	: out_(out)
{}

void LogicEquationSerializer::WriteOut(const std::set<VarId>& vars)
{
	for(std::set<VarId>::const_iterator it = vars.begin(); it != vars.end(); ++it)
	{
		out_ << "x" << *it << std::endl;
	}
}

void LogicEquationSerializer::WriteOut(const LogicVarContainer& vars)
{
	for(LogicVarContainer::const_iterator it = vars.begin(); it != vars.end(); ++it)
	{
		WriteOut(*it);
		out_ << std::endl;
	}
}

void LogicEquationSerializer::WriteOut(const LogicFormulaContainer& x)
{
	for(LogicFormulaContainer::const_iterator it = x.begin(); it != x.end(); ++it)
	{
		WriteOut(*it);
		out_ << std::endl;
	}
}

void LogicEquationSerializer::WriteOut(const LogicFormulaPtr& formula)
{
	if(!formula)
	{
		out_ << "?";
		return;
	}
	for(LogicFormula::const_iterator op_it = formula->begin(); op_it != formula->end(); ++op_it)
	{
		out_ << (op_it == formula->begin() ? "" : " ");
		const LogicOperand& op(*op_it);
		switch(op.type)
		{
		case Transalg::LogicOperandFormula:
			out_ << "?";
			break;
		case Transalg::LogicOperandConst:
			{
				if(op.value.False()) out_ << "0";
				else if(op.value.True()) out_ << "1";
				else out_ << "?";
			}
			break;
		case Transalg::LogicOperandVariable:
			{
				if(op.var)
					out_ << "x" << op.var->var_id;
				else
					out_ << "?";
			}
			break;
		case Transalg::LogicOperandFunction:
			{
				switch(op.func_type)
				{
				case LogicFunctionNot: out_ << "!"; break;
				case LogicFunctionAnd: out_ << "&"; break;
				case LogicFunctionOr:  out_ << "|"; break;
				case LogicFunctionXor: out_ << "^"; break;
				case LogicFunctionEquiv: out_ << "=="; break;
				case LogicFunctionTable: out_ << "<table>"; break;
				default:
					LOG_WARNING("LogicEquationSerializer:: unknown function type!");
				}
			}
			break;
		}
	}
}

void LogicEquationSerializer::WriteOut(const LogicVarPtr& var)
{
	if(var)
	{
		out_ << "x" << var->var_id << " = ";
		WriteOut(var->var_value);
	}
	else
		out_ << "?";
}

namespace
{

//! Каждые encode_word_size бита кодируем шетнадцатиричным числом
void HexStringEncoding(std::ostream& out, const LogicVarContainer& vars, std::size_t encode_word_size)
{
	unsigned c(0);
	const std::size_t width = encode_word_size / 4;
	out << std::setfill('0') << std::hex;
	for(std::size_t i(0); i < vars.size(); ++i)
	{
		const LogicFormulaPtr& value(vars[i]->var_value);
		if(!value || !value->IsConst())
		{
			out << "?";
			i += encode_word_size - i % encode_word_size;
			continue;
		}
		const unsigned pos = i % encode_word_size;
		if(value->back().value.True())
			c |= (1 << pos);
		if(pos == encode_word_size - 1)
		{
			out << std::setw(width) << c;
			c = 0;
		}
	}
}

} // local namespace

std::string LogicEquationSerializer::GetHexStringEncode(const LogicVarContainer& vars, std::size_t encode_word_size)
{
	std::stringstream ss;
	HexStringEncoding(ss, vars, encode_word_size);
	return ss.str();
}

} // namespace Transalg
