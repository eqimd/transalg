#pragma once

#include "Types.h"

namespace Transalg
{

class LogicEquationSerializer
{
public:

	LogicEquationSerializer(std::ostream& out);

	void WriteOut(const std::set<VarId>& vars);
	void WriteOut(const LogicVarPtr& var);
	void WriteOut(const LogicVarContainer& vars);
	void WriteOut(const LogicFormulaPtr& x);
	void WriteOut(const LogicFormulaContainer& x);

	//! Выводим значения переменных в виде шестнадцатиричной строки.
	//! Работает с константами или переменными, значения которых означены константой.
	static std::string GetHexStringEncode(const LogicVarContainer& vars, std::size_t encode_word_size);

private:

	std::ostream& out_;
};

} // namespace Transalg
