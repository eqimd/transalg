#pragma once

#include "Types.h"
#include "Statement.h"

namespace Transalg
{

//! Список инструкций ТА-программы, имеющих общую область видимости - блок
class StatementBlock: public Statement
{
	friend class Parser;
public:

	StatementBlock(const StatementBlock* context, unsigned line, StatementFunctionDefinition* func_ptr)
		: Statement(context, stmtBlock, line)
		, func_ptr_(func_ptr)
	{}

	StatementContainer::const_iterator begin() const {return statements_.begin();}

	StatementContainer::const_iterator end() const {return statements_.end();}

	StatementFunctionDefinition* GetFunction() const {return func_ptr_;}

	std::size_t GetStatementsCount() const {return statements_.size();}

private:

	//! Список инструкций, содержащихся в блоке
	StatementContainer statements_;

	//! Указатель на функцию, к которой относится блок (может отсутствовать)
	StatementFunctionDefinition* const func_ptr_;
};

typedef boost::shared_ptr<StatementBlock> StatementBlockPtr;

} // namespace Transalg
