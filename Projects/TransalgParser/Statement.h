#pragma once

#include "Types.h"

namespace Transalg
{

enum StatementType
{
	//! Произвольная инструкция ТА-программы
	stmtStatement,
	//! Определение переменной ТА-программы
	stmtVarDefinition,
	//! Определение массива переменных
	stmtArrayDefinition,
	//! Определение функции
	stmtFunctionDefinition, 
	//! Выржение, в том числе операция присваивания и вызов функции
	stmtExpression,
	//! Оператор цикла (for)
	stmtFor,
	//! Оператор условного перехода (if)
	stmtIf,
	//! Оператор выхода из функции (return)
	stmtReturn,
	//! Составной оператор (блок)
	stmtBlock,
	//! Табличное представление дискретной функции
	stmtTable
};

class Statement 
{

public:
	Statement(const StatementBlock* context, StatementType type, unsigned line)
		: statement_type_(type)
		, context_(context)
		, line_(line)
	{}

	virtual ~Statement() {}
	
	//! Тип инструкции
	StatementType GetStatementType() const {return statement_type_;}

	//! Номер строки исходного текста
	int GetLine() const {return line_;}

	//! Указатель на родительский контекст
	const StatementBlock* GetContext() const {return context_;}

protected:

	const StatementBlock* context_;
	StatementType statement_type_;
	unsigned line_;

};

typedef boost::shared_ptr<Statement> StatementPtr;
typedef std::vector<StatementPtr> StatementContainer;
typedef boost::shared_ptr<StatementContainer> StatementContainerPtr;

} // namespace Transalg
