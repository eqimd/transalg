#pragma once

#include "Types.h"
#include "Statement.h"
#include "StatementBlock.h"
#include "Expression.h"

namespace Transalg
{

class StatementFor: public Statement
{
	friend class Parser;
public:

	StatementFor(const StatementBlock* context, unsigned line)
		: Statement(context, stmtFor, line)
	{}

	StatementPtr GetInitStatement() const;

	ExpressionPtr GetStepExpression() const;

	ExpressionPtr GetConditionExpression() const;

	StatementPtr GetBody() const;

private:

	//! Инструкция, выполняющаяся на шаге инициализации
	StatementPtr init_;
	//! Инструкция, выполняющаяся после каждой итерации цикла
	ExpressionPtr step_;
	//! Условие
	ExpressionPtr condition_;
	//! Тело цикла
	StatementPtr body_;
};

typedef boost::shared_ptr<StatementFor> StatementForPtr;

} // namespace Transalg
