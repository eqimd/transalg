#pragma once

#include "Types.h"
#include "Statement.h"
#include "StatementBlock.h"
#include "Expression.h"

namespace Transalg
{

class StatementFunctionDefinition: public Statement
{
	friend class Parser;
public:

	StatementFunctionDefinition(const StatementBlock* context, unsigned line, 
		DataType data_type, const std::string id);

	DataType GetDataType() const;

	const std::string& GetId() const;

	std::size_t GetArgsCount() const;

	DataType GetArgDataType(unsigned arg_num) const;

	const std::string& GetArgId(unsigned arg_num) const;

	const std::vector<unsigned>& GetArgIndexes(unsigned arg_num) const;

	StatementBlockPtr GetBody() const;

private:
	//! Тип возвращаемого значения
	DataType data_type_;
	//! Идентификатор функции
	std::string id_;
	//! Типы аргументов
	std::vector<DataType> args_type_;
	//! Идентификаторы аргументов
	std::vector<std::string> args_id_;
	//! Индексы, если соотв. аргумент является массивом
	std::vector<std::vector<unsigned> > arg_indexes_;
	//! Тело функции - блок
	StatementBlockPtr block_;
};

typedef boost::shared_ptr<StatementFunctionDefinition> StatementFunctionDefinitionPtr;

} // namespace Transalg
