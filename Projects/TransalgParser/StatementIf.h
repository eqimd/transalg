#pragma once

#include "Types.h"
#include "Statement.h"
#include "Expression.h"

namespace Transalg
{

class StatementIf: public Statement
{
	friend class Parser;
public:

	explicit StatementIf(const StatementBlock* context, unsigned line)
		: Statement(context, stmtIf, line)
	{}

	ExpressionPtr GetCondition() const;

	StatementPtr GetTrueBranch() const;

	StatementPtr GetFalseBranch() const;

private:

	//! Условие 
	ExpressionPtr condition_;
	//! True-ветвь
	StatementPtr true_branch_;
	//! False-ветвь
	StatementPtr false_branch_;

};

typedef boost::shared_ptr<StatementIf> StatementIfPtr;

} // namespace Transalg
