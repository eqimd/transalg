#pragma once

#include "Token.h"

namespace Transalg
{

const std::string spaces = " \r\n\t";
const std::string decimal_digits = "0123456789";
const std::string hex_digits = "0123456789abcdefABCDEF";
const std::string delimeters = ",;{}()[]:\\";
const std::string letters = "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ";

inline bool isLetter(char t) {
	return letters.find(tolower(t)) != std::string::npos;
}

inline bool isNumeric(char t) {
	return decimal_digits.find(t) != std::string::npos;
}

inline bool isHexDigit(char t){
	return hex_digits.find(t) != std::string::npos;
}

inline bool isBitDigit(char t){
	return (t == '0') || (t == '1');
}

inline bool isSpace(char t) {
	return spaces.find(t) != std::string::npos;
}

inline bool isDelimeter(char t) {
	return delimeters.find(t) != std::string::npos;
}

inline bool isSign(char t){
	return std::string("-+").find(t) != std::string::npos;
}

inline bool isOperator(char t){
	return (std::string("-+*/%=<>!&|^").find(t) != std::string::npos);
}
inline bool isCompare(char t){
	return std::string("<>=").find(t) != std::string::npos;
}

//! Лексический анализатор ТА-программ
//! Разбирает текст программы на токены и отдает их наружу
class Lexer
{
public:

	Lexer();
	
	bool ParseTokens(const std::string& program_text, TokenStream& tokens);

private:

	Token GetNextToken();

	inline bool IsEof() const;
	inline char IncrementCursor();
	inline char CursorValue() const;
	inline char CursorNextValue() const;

	Token ParseNumber();
	Token ParseId();
	Token ParseOperator();
	void SkipSpacesAndComments();

private:

	//! Курсор, указывает на обозреваемый в настоящий момент символ ТА-программы
	std::string::const_iterator begin_;
	std::string::const_iterator end_;
	std::string::const_iterator cursor_;

	unsigned line_;
	unsigned col_;
};

void WriteOut(std::ostream& out, const TokenStream& tokens);

} // namespace Transalg
