#include "stdafx.h"
#include "Lexer.h"
#include "../ParseError.h"

namespace Transalg
{

Lexer::Lexer()
	: line_(1)
	, col_ (1)
{}

bool Lexer::ParseTokens(const std::string& program_text, TokenStream& tokens)
{
	tokens.clear();
	begin_ = program_text.begin();
	end_ = program_text.end();
	for(cursor_ = begin_; cursor_ != end_;)
	{
		tokens.push_back(GetNextToken());
	}
	return true;
}

bool Lexer::IsEof() const
{
	return cursor_ == end_;
}

char Lexer::CursorValue() const
{
	return (IsEof() ? 0 : *cursor_);
}

char Lexer::CursorNextValue() const
{
	return (!IsEof() && (cursor_ + 1 != end_) ? *(cursor_ + 1) : 0);
}

char Lexer::IncrementCursor()
{
	if(IsEof())
	{
		return 0;
	}
	if(*cursor_ == '\n')
	{
		++line_;
		col_ = 0;
	}
	++col_;
	++cursor_;
	return (IsEof() ? 0 : *cursor_);
}

void Lexer::SkipSpacesAndComments()
{
	bool ok = false;
	while(!IsEof() && !ok)
	{
		while(isSpace(CursorValue()))
			IncrementCursor();

		if(CursorValue() == '/')
		{
			const char symbol = CursorNextValue();
			switch(symbol)
			{
			case '*':
				IncrementCursor();
				while(IncrementCursor())
				{
					if(CursorValue() == '*' && CursorNextValue() == '/')
					{
						IncrementCursor();
						IncrementCursor();
						break;
					}
				}
				break;
			case '/':
				IncrementCursor();
				while(IncrementCursor())
				{
					if(CursorValue() == '\n')
						break;
				}
				break;
			default:
				ok = true;
			}
		}
		else
		{
			ok = true;
		}
	}
}

Token Lexer::ParseNumber()
{
	char symbol;
	//! Шестнадцатиричная запись
	if(CursorValue() == '0' && CursorNextValue() == 'x')
	{
		IncrementCursor();
		std::string token_text("0x");
		while((symbol = IncrementCursor()) && isHexDigit(symbol))
		{
			token_text.push_back(symbol);
		}
		return (token_text.size() > 2 ? Token(ttNumber, tcHexNumber, token_text, line_, col_) 
			: Token(ttError, tcNone, token_text, line_, col_));
	}

	//! Двоичная запись
	if(CursorValue() == '0' && CursorNextValue() == 'b')
	{
		IncrementCursor();
		std::string token_text("0b");
		while((symbol = IncrementCursor()) && isBitDigit(symbol))
		{
			token_text.push_back(symbol);
		}
		return (token_text.size() > 2 ? Token(ttNumber, tcBitNumber, token_text, line_, col_) 
			: Token(ttError, tcNone, token_text, line_, col_));
	}

	//! Десятичная запись
	std::string token_text(1, *cursor_);
	while((symbol = IncrementCursor()) && isNumeric(symbol))
	{
		token_text.push_back(symbol);
	}
	
	return Token(ttNumber, tcDecNumber, token_text, line_, col_);
}

Token Lexer::ParseId()
{
	char symbol;
	std::string token_text(1, *cursor_);
	while((symbol = IncrementCursor()) && (isLetter(symbol) || isNumeric(symbol)))
	{
		token_text.push_back(symbol);
	}
	
	TokenType token_type(ttId);
	TokenCode token_code(tcNone);

		 if(token_text == "void")   token_type = ttDataType, token_code = tcVoid;
	else if(token_text == "int")    token_type = ttDataType, token_code = tcInt; 
	else if(token_text == "bit")    token_type = ttDataType, token_code = tcBit;
	else if(token_text == "define") token_type = ttKeyWord, token_code = tcDefine;
	else if(token_text == "__in")   token_type = ttAttribute, token_code = tcIn; 
	else if(token_text == "__out")  token_type = ttAttribute, token_code = tcOut;
	else if(token_text == "__mem")  token_type = ttAttribute, token_code = tcMem;
	else if(token_text == "table")  token_type = ttKeyWord, token_code = tcTable;
	else if(token_text == "for")    token_type = ttKeyWord, token_code = tcFor;
	else if(token_text == "if")     token_type = ttKeyWord, token_code = tcIf;
	else if(token_text == "else")   token_type = ttKeyWord, token_code = tcElse;
	else if(token_text == "return") token_type = ttKeyWord, token_code = tcReturn;
	//else if(token_text == "sum" || token_text == "mul" || token_text == "concat") 
	//	token_type = ttFunction;

	return Token(token_type, token_code, token_text, line_, col_);
}

Token Lexer::ParseOperator()
{
	char symbol(*cursor_);
	TokenType token_type(ttOperator);
	TokenCode token_code(tcNone);
	std::string token_text(1, symbol);

	switch(symbol)
	{
		case '+': token_code = tcSum; break;
		case '-': token_code = tcSub; break;
		case '*': token_code = tcMul; break;
		case '/': token_code = tcDiv; break;
		case '%': token_code = tcMod; break;
		case '!':
			if(CursorNextValue() == '=')
			{
				token_text.push_back(IncrementCursor());
				token_code = tcNotEqual;
			}
			else
			{
				token_code = tcNot;
			}
			break;

		case '&': token_code = tcAnd; break;
		case '|': 
			if(CursorNextValue() == '|')
			{
				token_text.push_back(IncrementCursor());
				token_code = tcConcat;
			}
			else
			{
				token_code = tcOr;
			}
			break;
		case '^': token_code = tcXor; break;
		case '=':
			if(CursorNextValue() == '=')
			{
				token_text.push_back(IncrementCursor());
				token_code = tcEqual;
			}
			else
			{
				token_code = tcAssign;
			}
			break;

		case '<':
			if(CursorNextValue() == '<')
			{
				token_text.push_back(IncrementCursor());
				if(CursorNextValue() == '<')
				{
					token_text.push_back(IncrementCursor());
					token_code = tcLeftCircleShift;
				}
				else if(CursorNextValue() == '*')
				{
					token_text.push_back(IncrementCursor());
					token_code = tcLeftSaveShift;
				}
				else
				{
					token_code = tcLeftShift;
				}
			}
			else if(CursorNextValue() == '=')
			{
				token_text.push_back(IncrementCursor());
				token_code = tcLessEq;
			}
			else
			{
				token_code = tcLess;
			}
			break;

		case '>':
			if(CursorNextValue() == '>')
			{
				token_text.push_back(IncrementCursor());
				if(CursorNextValue() == '>')
				{
					token_text.push_back(IncrementCursor());
					token_code = tcRightCircleShift;
				}
				else if(CursorNextValue() == '*')
				{
					token_text.push_back(IncrementCursor());
					token_code = tcRightSaveShift;
				}
				else
				{
					token_code = tcRightShift;
				}
			}
			else if(CursorNextValue() == '=')
			{
				token_text.push_back(IncrementCursor());
				token_code = tcGreatEq;
			}
			else
			{
				token_code = tcGreat;
			}
			break;
		default: 
			token_type = ttError;
	}
	IncrementCursor();
	return Token(token_type, token_code, token_text, line_, col_);
}

Token Lexer::GetNextToken()
{
	SkipSpacesAndComments();
	//! Конец программы
	if(IsEof())
	{
		return Token(ttEof, tcNone, "", line_, col_);
	}

	//! Целочисленная константа
	if(isNumeric(*cursor_))
	{
		return ParseNumber();
	}

	//! Идентификатор
	if(isLetter(*cursor_))
	{
		return ParseId();
	}

	//! Разделитель
	if(isDelimeter(*cursor_))
	{
		TokenType token_type(ttDelimiter);
		TokenCode token_code(tcNone);
		switch(*cursor_)
		{
			case ',': token_code = tcComma; break;
			case ';': token_code = tcSemicolumn; break;
			case ':': token_code = tcColon; break;
			case '(': token_code = tcLeftRoundBrace; break;
			case ')': token_code = tcRightRoundBrace; break;
			case '[': token_code = tcLeftSquareBrace; break;
			case ']': token_code = tcRightSquareBrace; break;
			case '{': token_code = tcLeftBrace; break;
			case '}': token_code = tcRightBrace; break;
			default: token_type = ttError;
		}
		const std::string token_text(1, *cursor_);
		IncrementCursor();
		return Token(token_type, token_code, token_text, line_, col_);
	}

	//! Оператор
	if(isOperator(*cursor_))
	{
		return ParseOperator();
	}

	//! Неизвестный символ
	IncrementCursor();
	return Token(ttError, tcNone, "", line_, col_);
}

void WriteOut(std::ostream& out, const TokenStream& tokens)
{
	for(TokenStream::const_iterator token_it = tokens.begin();
		token_it != tokens.end();
		++token_it)
	{
		out << token_it->text << std::endl;
	}
}

} // namespace Transalg
