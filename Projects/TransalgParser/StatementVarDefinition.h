#pragma once

#include "Types.h"
#include "Statement.h"
#include "Expression.h"

namespace Transalg
{

class StatementVarDefinition: public Statement
{
	friend class Parser;
public:
	
	explicit StatementVarDefinition(const StatementBlock* context, unsigned line, 
		Attribute attr, DataType type, const std::string& id, ExpressionPtr init_value = ExpressionPtr())
		: Statement(context, stmtVarDefinition, line)
		, attribute_(attr)
		, data_type_(type)
		, id_(id)
		, init_value_(init_value)
	{}

	Attribute GetAttribute() const;

	DataType GetDataType() const;

	const std::string& GetId() const;

	ExpressionPtr GetInitValue() const;

private:

	//! Атрибут {__in, __out, __mem}
	Attribute attribute_;
	//! Тип данных переменной
	DataType data_type_;
	//! Идентификатор переменной
	std::string id_;
	//! Инициализирующее выражение (если есть)
	ExpressionPtr init_value_;
};

typedef boost::shared_ptr<StatementVarDefinition> StatementVarDefinitionPtr;

} // namespace Transalg
