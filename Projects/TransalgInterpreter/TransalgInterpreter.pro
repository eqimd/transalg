TEMPLATE = lib
TARGET = TransalgInterpreter

include(../../Mk/Common/common.pri)

CONFIG += staticlib

CONFIG(debug, debug|release) {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$DEBUG_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$DEBUG_FOLDER"
} else {
	OBJECTS_DIR += "$$BASEPATH/$$OBJ_FOLDER/$$RELEASE_FOLDER/$$TARGET"
	DESTDIR = "$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
	LIBS += -L"$$BASEPATH/$$LIB_FOLDER/$$RELEASE_FOLDER"
}

# Дополнительные каталоги включения для данного проекта
INCLUDEPATH += src . ..

PRECOMPILED_HEADER = src/stdafx.h
PRECOMPILED_SOURCE = src/stdafx.cpp

HEADERS += \
	IInterpreter.h \
	Interpreter.h \
	src/ALU.h \
	src/DataObject.h \
	src/DataTypeBit.h \
	src/DataTypeInteger.h \
	src/DataTypes.h \
	src/DataTypeVoid.h \
	src/IALU.h \
	src/IInterpreterImpl.h \
	src/InterpreterImpl.h \
	src/SemanticLibrary.h

SOURCES += \
	src/ALU.cpp \
	src/DataObject.cpp \
	src/DataTypeBit.cpp \
	src/DataTypeInteger.cpp \
	src/DataTypes.cpp \
	src/Interpreter.cpp \
	src/InterpreterImpl.cpp \
	src/SemanticLibrary.cpp

