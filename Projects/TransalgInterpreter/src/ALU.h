#pragma once

#include "IALU.h"
#include "IInterpreterImpl.h"
#include <TransalgFormats/EncodeFactory.h>

namespace Transalg
{

//! Класс, отвечающий за вычисление выражений
class ALU: public IALU
{
public:

	explicit ALU(IInterpreterImpl* instance, EncodeFactoryPtr encode)
		: instance_(instance) 
		, encode_(encode)
	{}

	//! Вычислить выражение
	VoidPtr ExecuteExpression(ExpressionPtr expression, const DataObjectContext* context);

private:

	//! Проверка по идентификатору есть ли подходящая встроенная функция и если есть, то выполнить ее
	//! В случае, если встроенная функция была найдена и успешно выполнена, то возвращаем true
	bool IsBuiltInFunction(const ExpressionOperand& operand, std::stack<VoidPtr>& magazin);
	void GetFunctionArguments(unsigned arg_count, VoidContainer& arguments, std::stack<VoidPtr>& magazin) const;

	bool IsHexLiteral(const std::string& id) const;

	//! Операция присваивания
	VoidPtr assign(VoidPtr lvalue, VoidPtr rvalue);
	void bit_assign(BitPtr& lvalue, const BitPtr& rvalue);

	//! Целочисленные арифметические операции
	VoidPtr sum(VoidPtr x, VoidPtr y);
	VoidPtr sum(VoidPtr x, VoidPtr y, IntegerPtr m);
	VoidPtr sub(VoidPtr x, VoidPtr y);
	VoidPtr mul(VoidPtr x, VoidPtr y);
	VoidPtr mul(VoidPtr x, VoidPtr y, IntegerPtr m);
	VoidPtr div(VoidPtr x, VoidPtr y);
	VoidPtr mod(VoidPtr x, VoidPtr y);

	//! Операции сравнения
	BitPtr great(VoidPtr x, VoidPtr y);
	BitPtr ge(VoidPtr x, VoidPtr y);
	BitPtr less(VoidPtr x, VoidPtr y);
	BitPtr le(VoidPtr x, VoidPtr y);
	BitPtr equiv(VoidPtr x, VoidPtr y);
	BitPtr neq(VoidPtr x, VoidPtr y);

	//! Побитовые логические операции
	VoidPtr not_(VoidPtr x);
	VoidPtr and_(VoidPtr x, VoidPtr y);
	VoidPtr or_(VoidPtr x, VoidPtr y);
	VoidPtr xor_(VoidPtr x, VoidPtr y);

	//! Конкатенация битовых векторов
	VoidPtr concat(VoidPtr x, VoidPtr y);

	//! Побитовые операции сдвига
	VoidPtr LeftShift(VoidPtr x, IntegerPtr pos);
	VoidPtr RightShift(VoidPtr x, IntegerPtr pos);
	VoidPtr LeftCircleShift(VoidPtr x, IntegerPtr pos);
	VoidPtr RightCircleShift(VoidPtr x, IntegerPtr pos);
	VoidPtr LeftSaveShift(VoidPtr x, IntegerPtr pos);
	VoidPtr RightSaveShift(VoidPtr x, IntegerPtr pos);

private:

	//! Пропозициональный код
	EncodeFactoryPtr encode_;
	IInterpreterImpl* const instance_;
};

} // namespace Transalg
