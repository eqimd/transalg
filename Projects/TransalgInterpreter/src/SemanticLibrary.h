#pragma once

#include "DataTypes.h"
#include <TransalgFormats/EncodeFactory.h>

namespace Transalg
{
namespace SemanticLib
{
	//! Целочисленное умножение
	BitVectorPtr multiply(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное умножение, размер результата ограничен bit_size бит
	BitVectorPtr multiply(BitVectorPtr x, BitVectorPtr y, std::size_t bit_size, EncodeFactory& encode);

	//! Целочисленная сумма
	BitVectorPtr sum(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленная сумма, размер результата ограничен bit_size бит
	BitVectorPtr sum(BitVectorPtr x, BitVectorPtr y, std::size_t bit_size, EncodeFactory& encode);

	//! Целочисленная разность
	BitVectorPtr sub(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Побитовая коньюнкция булевых векторов
	BitVectorPtr and_(BitVectorPtr x, BitVectorPtr y);

	//! Конъюнкция каждого бита вектора 'x' с битом, кодируемым булевой формулой 'y'
	BitVectorPtr and_(BitVectorPtr x, LogicFormulaPtr y);

	//! Побитовая дизъюнкция булевых векторов
	BitVectorPtr or_(BitVectorPtr x, BitVectorPtr y);

	//! Дизъюнкция каждого бита вектора 'x' с битом, кодируемым булевой формулой 'y'
	BitVectorPtr or_(BitVectorPtr x, LogicFormulaPtr y);

	//! Побитовое исключающее ИЛИ булевых векторов
	BitVectorPtr xor_(BitVectorPtr x, BitVectorPtr y);

	//! Исключающее ИЛИ каждого бита вектора 'x' с битом, кодируемым булевой формулой 'y'
	BitVectorPtr xor_(BitVectorPtr x, LogicFormulaPtr y);

	//! Побитовое отрицание
	BitVectorPtr not_(BitVectorPtr x);

	//! Целочисленное >
	LogicFormulaPtr great(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное >, операнд 'y' - константа
	LogicFormulaPtr great_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное >=
	LogicFormulaPtr ge(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное >=, операнд 'y' - константа
	LogicFormulaPtr ge_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное <
	LogicFormulaPtr less(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное <, операнд 'y' - константа
	LogicFormulaPtr less_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное <=
	LogicFormulaPtr le(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное <=, операнд 'y' - константа
	LogicFormulaPtr le_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное ==
	LogicFormulaPtr eq(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное ==, операнд 'y' - константа
	LogicFormulaPtr eq_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное !=
	LogicFormulaPtr neq(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Целочисленное !=, операнд 'y' - константа
	LogicFormulaPtr neq_const(BitVectorPtr x, BitVectorPtr y, EncodeFactory& encode);

	//! Позиция старшего (ненулевого) бита
	unsigned MajorBitPos(unsigned x);

	//! Циклический сдвиг числа x влево на pos бит
	int CircleLeftShift(int x, unsigned pos);

	//! Циклический сдвиг числа x вправо на pos бит
	int CircleRightShift(int x, unsigned pos);

	//! Сдвиг с "залипанием" числа х влево на pos бит
	int SaveLeftShift(int x, unsigned pos);

	//! Сдвиг с "залипанием" числа х вправо на pos бит
	int SaveRightShift(int x, unsigned pos);

	//! Удалить нулевые старшие биты в векторе
	void RemoveLeadFalseBits(LogicFormulaContainer& vec);

	std::string PrintBits(unsigned x);

} // namespace SemanticLib
} // namespace Transalg
