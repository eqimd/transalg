#include "stdafx.h"
#include "DataTypeInteger.h"
#include "DataTypeBit.h"
#include "ALU.h"
#include "SemanticLibrary.h"

namespace Transalg
{

VoidPtr ALU::ExecuteExpression(ExpressionPtr expression, const DataObjectContext* context)
{
	if(!expression)
		throw std::invalid_argument("ALU::ExecuteExpression: expression - null pointer.");
	if(!context)
		throw std::invalid_argument("ALU::ExecuteExpression: context - null pointer.");

	std::stack<VoidPtr> magazin;

	for(unsigned i = 0, size = expression->GetOperandsCount(); i < size; ++i)
	{
		const ExpressionOperand& operand = expression->GetOperand(i);
		switch(operand.type_)
		{
		case OperandTypeConst:
			{
				assert(operand.arg_count_ == 0);
				if(IsHexLiteral(operand.id_))
				{
					magazin.push(boost::make_shared<BitVector>(operand.id_));
				}
				else
				{
					//! Константа - целое число. Помещаем в стек.
					const unsigned value = boost::lexical_cast<unsigned>(operand.id_);
					magazin.push(boost::make_shared<Integer>(value));
				}
			}
			break;
		case OperandTypeVariable:
			{
				assert(operand.arg_count_ == 0);
				//! Поиск переменной в контексте по идентификатору
				DataObjectPtr data = context->FindObject(operand.id_);
				if(!data)
				{
					throw std::runtime_error("ALU::ExecuteExpression: variable '" + operand.id_ + "' is not_ found in the context!");
				}
				//! Убедимся, что найденный объект - это переменная
				if(DataObjectVarPtr var_data = boost::dynamic_pointer_cast<DataObjectVar>(data))
				{
					magazin.push(var_data->data);
				}
				//! fixme: неправильный тип объекта?
				else if(DataObjectArrayPtr array_data = boost::dynamic_pointer_cast<DataObjectArray>(data))
				{
					magazin.push(array_data->GetValue());
				}
				else
					throw std::runtime_error("ALU::ExecuteExpression: data object is not_ a variable!");
			}
			break;
		case OperandTypeArrayVariable:
			{
				DataObjectPtr data = context->FindObject(operand.id_);
				if(!data)
				{
					throw std::runtime_error("ALU::ExecuteExpression: array '" + operand.id_ + "' is not_ found in the context!");
				}
				//! Убедимся, что найденный объект - это массив
				if(DataObjectArrayPtr array_data = boost::dynamic_pointer_cast<DataObjectArray>(data))
				{
					//! Проверим, что число индексов не превышает требуемое
					if(operand.arg_count_ > array_data->GetIndexCount())
					{
						throw std::invalid_argument("ALU: passed too many array indexes.");
					}

					//! Индексы не указаны вовсе - допускается только для массивов типа bit
					if(operand.arg_count_ == 0)
					{
						if(array_data->GetDataType() != Transalg::dtBit)
							throw std::invalid_argument("ALU::ExecuteExpression: array indexes must be specified.");
						magazin.push(array_data->GetValue());
						continue;
					}

					//! Для типа Integer должны быть указаны ВСЕ индексы массива
					//! Для типа Bit часть индексов может быть опущена (работа с подмассивом бит)
					if(array_data->GetDataType() == Transalg::dtInt && operand.arg_count_ < array_data->GetIndexCount())
					{
						throw std::invalid_argument("ALU: for the Integer type all indexes must be specified.");
					}

					std::vector<unsigned> arg_index(operand.arg_count_);
					for(unsigned i = 1; i <= operand.arg_count_; ++i)
					{
						//! Индексы массива - это всегда целые беззнаковые числа
						IntegerPtr index = toInt(magazin.top());
						if(!(index && index->GetValue() >= 0))
							throw std::runtime_error("ALU::ExecuteExpression: invalid index of array.");
						arg_index[operand.arg_count_ - i] = static_cast<unsigned>(index->GetValue());
						magazin.pop();
					}
					//! Помещаем элемент массива в стек
					magazin.push(array_data->GetValue(arg_index));
				}
				else
					throw std::runtime_error("ALU::ExecuteExpression: data object is not_ an array!");
			}
			break;
		case OperandTypeFunction:
			{
				//! Встроенные функции
				if(IsBuiltInFunction(operand, magazin))
					continue;

				//! Ищем среди пользовательских функций, объявленных в тексте текущей ТА-программы
				DataObjectPtr data = context->FindObject(operand.id_);
				if(!data)
					throw std::runtime_error("ALU::ExecuteExpression: function '" + operand.id_ + "' is not_ found in the context!");
				
				if(DataObjectFunctionPtr func_data = boost::dynamic_pointer_cast<DataObjectFunction>(data))
				{
					if(operand.arg_count_ != func_data->GetArgCount())
						throw std::runtime_error("AlU::ExecuteExpression: function operand count is not_ equal of formal argument count of this function.");

					//! Извлекаем аргументы функции из стека
					//! Аргументом функции может быть константа, переменная или массив (др. функция?)
					VoidContainer arguments;
					GetFunctionArguments(operand.arg_count_, arguments, magazin);

					VoidPtr result = instance_->ExecFunctionCall(func_data, arguments);

					//! Функция не возвращает результат
					if(func_data->GetDataType() == Transalg::dtVoid)
					{
						assert(!result);
						//! Проверим, что вызов этой функции - это последний операнд выражения
						if(i + 1 != size)
						{
							throw std::runtime_error("ALU::ExecuteExpression: function that does not_ return value can not_ be an operand of expression!");
						}
						return VoidPtr();
					}

					if(!result)
						throw std::runtime_error("ALU::ExecuteExpression: function return null value.");

					if(result->GetDataType() != func_data->GetDataType())
					{
						std::stringstream msg;
						msg << "ALU::ExecuteExpression: unexpected data type of the function result." << std::endl;
						msg << "Line: " << ", Id: " << func_data->GetObjectId() << ", Expect: " << func_data->GetDataType() << ", Actual: " << result->GetDataType() << std::endl;
						throw std::runtime_error(msg.str());
					}

					magazin.push(result);
				}
				else
					throw std::runtime_error("ALU::ExecuteExpression: data object is not_ a function!");
			}
			break;
		
		case OperandTypeOperator:
			{
				//! Допускается 1 или 2 операдна
				assert(operand.arg_count_ >= 1 && operand.arg_count_ <= 2);
				//! Извлекаем из стека операнды
				VoidPtr right_operand = magazin.top();
				magazin.pop();
				VoidPtr left_operand;
				if(operand.arg_count_ == 2)
				{
					left_operand = magazin.top();
					magazin.pop();
				}

				const std::string& id(operand.id_);

				if(id == "=")      
				{
					magazin.push(assign(left_operand, right_operand));
				}
				if(id == "+")      magazin.push(sum(left_operand, right_operand));
				if(id == "-")      magazin.push(sub(left_operand, right_operand));
				if(id == "*")      magazin.push(mul(left_operand, right_operand));
				if(id == "/")      magazin.push(div(left_operand, right_operand));
				if(id == "%")      magazin.push(mod(left_operand, right_operand));
				if(id == ">")      magazin.push(great(left_operand, right_operand));
				if(id == ">=")     magazin.push(ge(left_operand, right_operand));
				if(id == "<")      magazin.push(less(left_operand, right_operand));
				if(id == "<=")     magazin.push(le(left_operand, right_operand));
				if(id == "==")     magazin.push(equiv(left_operand, right_operand));
				if(id == "!=")     magazin.push(neq(left_operand, right_operand));
				if(id == "!")      magazin.push(not_(right_operand));
				if(id == "&")      magazin.push(and_(left_operand, right_operand));
				if(id == "|")      magazin.push(or_(left_operand, right_operand));
				if(id == "^")      magazin.push(xor_(left_operand, right_operand));
				if(id == "||")     magazin.push(concat(left_operand, right_operand));
				if(id == "<<")     magazin.push(LeftShift(left_operand, toInt(right_operand)));
				if(id == ">>")     magazin.push(RightShift(left_operand, toInt(right_operand)));
				if(id == "<<<")    magazin.push(LeftCircleShift(left_operand, toInt(right_operand)));
				if(id == ">>>")    magazin.push(RightCircleShift(left_operand, toInt(right_operand)));
				if(id == "<<*")    magazin.push(LeftSaveShift(left_operand, toInt(right_operand)));
				if(id == ">>*")    magazin.push(RightSaveShift(left_operand, toInt(right_operand)));

			}
			break;
		default:
			throw std::runtime_error("ALU::ExecuteExpression: unknown operand type!");
		}

		//if(operand.line_ >= 64)
		//{
		//	std::cout << "line: " << operand.line_ << ", arg: " << operand.id_ << ", result: " << std::endl;
		//	PrintData(std::cout, magazin.top());
		//}

	} // for

	assert(magazin.size() == 1);
	return magazin.top();
}

bool ALU::IsHexLiteral(const std::string& id) const
{
	return (id.size() > 2 && id.substr(0, 2) == "0x");
}

bool ALU::IsBuiltInFunction(const ExpressionOperand& operand, std::stack<VoidPtr>& magazin)
{
	//! mul(x, y, m) - от произведения (x * y) берем m первых бит - остальные зануляем
	if(operand.id_ == "mul")
	{
		if(operand.arg_count_ == 3)
		{
			VoidContainer arguments;
			GetFunctionArguments(operand.arg_count_, arguments, magazin);
			magazin.push(mul(arguments[0], arguments[1], toInt(arguments[2])));
			return true;
		}
		else
			throw std::runtime_error("ALU::IsBuiltInFunction(): the 'mul' function must have 3 arguments.");
	}
	//! sum(x, y, m) - от суммы (x * y) берем m первых бит - остальные зануляем
	else if(operand.id_ == "sum")
	{
		if(operand.arg_count_ == 3)
		{
			VoidContainer arguments;
			GetFunctionArguments(operand.arg_count_, arguments, magazin);
			magazin.push(sum(arguments[0], arguments[1], toInt(arguments[2])));
			return true;
		}
		else
			throw std::runtime_error("ALU::IsBuiltInFunction(): the 'sum' function must have 3 arguments.");
	}
	//! assert(X) - добавляет в систему уравнений уравнение вида X = 1
	else if(operand.id_ == "assert")
	{
		//! Операндом функции assert может быть только булева формула формула
		VoidPtr arg = magazin.top();
		if(BitPtr arg_bit = boost::dynamic_pointer_cast<Bit>(arg))
		{
			encode_->constraints.push_back(arg_bit->GetValue());
		}
		else if(BitVectorPtr arg_bit_vec = boost::dynamic_pointer_cast<BitVector>(arg))
		{
			for(unsigned i(0); i < arg_bit_vec->GetSize(); ++i)
			{
				encode_->constraints.push_back(arg_bit_vec->GetBit(i)->GetValue());
			}
		}
		else
		{
			throw std::runtime_error("ALU::IsBuiltInFunction(): invalid type of argument for the 'assert' function.");
		}
		return true;
	}
	//! core_var(X) - добавляет переменные кода, связанные с переменной программы X, в множество ядровых переменных
	else if(operand.id_ == "core_var")
	{
		//! Аргументами функции addcore могут быть только переменные кода
		VoidPtr arg = magazin.top();
		if(BitPtr arg_bit = boost::dynamic_pointer_cast<Bit>(arg))
		{
			LogicFormulaPtr var_value(arg_bit->GetValue());
			if(var_value->IsVariable())
			{
				encode_->core_vars.insert(var_value->front().var);
			}
			//else
			//	LOG_WARNING("Argument of the 'core_var' function is not_ encode variable.");
		}
		else if(BitVectorPtr arg_bit_vec = boost::dynamic_pointer_cast<BitVector>(arg))
		{
			for(unsigned i(0); i < arg_bit_vec->GetSize(); ++i)
			{
				LogicFormulaPtr var_value(arg_bit_vec->GetBit(i)->GetValue());
				if(var_value->IsVariable())
				{
					encode_->core_vars.insert(var_value->front().var);
				}
				//else
				//	LOG_WARNING("Argument of the 'core_var' function is not_ encode variable.");
			}
		}
		else
		{
			throw std::runtime_error("ALU::IsBuiltInFunction(): invalid type of argument for the 'core_var' function.");
		}
		return true;
	}
	return false;
}

void ALU::GetFunctionArguments(unsigned arg_count, VoidContainer& arguments, std::stack<VoidPtr>& magazin) const
{
	for(unsigned i = 0; i < arg_count; ++i)
	{
		arguments.push_back(magazin.top());
		magazin.pop();
	}
	std::reverse(arguments.begin(), arguments.end());
}

VoidPtr ALU::assign(VoidPtr lvalue, VoidPtr rvalue)
{
	if(!lvalue || !rvalue)
		throw std::invalid_argument("ALU::assign: argument - null pointer.");

	if(IntegerPtr lv = boost::dynamic_pointer_cast<Integer>(lvalue))
	{
		const int rv = toInt(rvalue)->GetValue();
		lv->SetValue(rv);
	}
	else if(BitPtr lv = boost::dynamic_pointer_cast<Bit>(lvalue))
	{
		BitPtr rv = toBit(rvalue);
		bit_assign(lv, rv);
	}
	else if(BitVectorPtr lv = boost::dynamic_pointer_cast<BitVector>(lvalue))
	{
		BitVectorPtr rv = toBitVector(rvalue);
		const std::size_t size = std::min(lv->GetSize(), rv->GetSize());

		for(unsigned i = 0; i < size; ++i)
		{
			bit_assign((*lv)[i], (*rv)[i]);
		}
		//! Оставшиеся старшие биты в lv означиваем нулями
		for(unsigned j = size; j < lv->GetSize(); ++j)
			(*lv)[j]->SetValue(EncodeFactory::False());
	}
	else
		throw std::runtime_error("ALU::assign(): bad dynamic pointer cast.");

	return lvalue;
}

void ALU::bit_assign(BitPtr& lvalue, const BitPtr& rvalue)
{
	LogicFormulaPtr left_formula = lvalue->GetValue();
	LogicFormulaPtr right_formula = rvalue->GetValue();
	if(!left_formula || !right_formula)
		throw std::runtime_error("ALU::bit_assign(): argument - null pointer.");

	//! Идет процесс интерпретации условного оператора - присваивания до конца не делаем
	if(instance_->IsBranchMode() && !instance_->IsLocalVar(lvalue))
	{
		SuggestContainer* var_suggests = instance_->GetSuggestBranchVars();
		if(!var_suggests)
			throw std::runtime_error("ALU::bit_assign: var_suggests - null pointer.");

		//! Возможен случай, когда одна и та же переменная программы в одной ветви условного оператора будет меняться несколько раз.
		//! В этом случае запоминаем последнее значение данной переменной программы.
		SuggestContainer::iterator var_suggest_it = instance_->FindVarInSuggest(lvalue, *var_suggests);
		if(var_suggest_it != var_suggests->end())
		{
			var_suggest_it->second = right_formula;
		}
		else
			var_suggests->push_back(Suggest(lvalue, right_formula));
	}
	//! Слева переменная кода
	else if(left_formula->IsVariable())
	{
		LogicVarPtr var = left_formula->front().var;

		//! fixme: переменные выхода - это прежде всего переменные программы
		//! Переменные кода, кодирующие выход можно выделять по факту из цейтиновских переменных
		if(var->var_type == Transalg::LogicVarOutput)
		{
			var->var_value = right_formula;
		}
		//! Переменная программы меняет ссылку на переменную кода
		else if(right_formula->IsVariable())
		{
			lvalue->SetValue(right_formula);
		}
		//! Слева  - переменная кода, которая инициализирована константой или функцией над переменными кода
		//! Справа - функция над переменными кода
		else
		{
			//! Создаем новую переменную кода
			lvalue->SetValue(boost::make_shared<LogicFormula>(encode_->CreateVar(right_formula)));
		}
	}
	//! Слева константа или функция над переменными кода
	else
	{
		lvalue->SetValue(right_formula);
	}
}

VoidPtr ALU::sum(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::sum: argument - null pointer.");

	//! Сложение для целочисленных констант
	if(x->IsInt() && y->IsInt())
	{
		IntegerPtr ix = toInt(x);
		IntegerPtr iy = toInt(y);
		const int value = ix->GetValue() + iy->GetValue();
		return boost::make_shared<Integer>(value);
	}
	//! Пропозициональный код
	return SemanticLib::sum(toBitVector(x), toBitVector(y), *encode_);
}

VoidPtr ALU::sum(VoidPtr x, VoidPtr y, IntegerPtr m)
{
	if(!x || !y || !m)
		throw std::invalid_argument("ALU::sum: argument - null pointer.");
	if(m->GetValue() <= 0)
		throw std::invalid_argument("ALU::sum: argument 'mod' must be a positive.");

	//! Длина результирующего вектора не может превышать 32 бит
	const std::size_t mod = std::min<std::size_t>(m->GetValue(), 32);
	//! Сложение для целочисленных констант
	if(x->IsInt() && y->IsInt())
	{
		IntegerPtr ix = toInt(x);
		IntegerPtr iy = toInt(y);

		//! Битовая маска, ограничивающая результат
		unsigned mask = 0;
		for(unsigned j = 0; j < mod; ++j)
			mask |= (static_cast<unsigned>(1) << j);

		const int value = (ix->GetValue() + iy->GetValue()) & mask;
		return boost::make_shared<Integer>(value);
	}
	//! Пропозициональный код
	return SemanticLib::sum(toBitVector(x), toBitVector(y), mod, *encode_);
}

VoidPtr ALU::sub(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::sub: argument - null pointer.");

	//! Вычитание для целочисленных констант
	if(x->IsInt() && y->IsInt())
	{
		IntegerPtr ix = toInt(x);
		IntegerPtr iy = toInt(y);
		const int value = ix->GetValue() - iy->GetValue();
		return boost::make_shared<Integer>(value);
	}
	else
		throw std::invalid_argument("ALU::sub: bad type of argument.");
	return VoidPtr();
}

VoidPtr ALU::mul(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::mul: argument - null pointer.");

	//! Для целочисленных констант
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		return boost::make_shared<Integer>(x_value * y_value);
	}
	//! Пропозициональный код
	return SemanticLib::multiply(toBitVector(x), toBitVector(y), *encode_);
}

VoidPtr ALU::mul(VoidPtr x, VoidPtr y, IntegerPtr m)
{
	if(!x || !y || !m)
		throw std::invalid_argument("ALU::mul: argument - null pointer.");
	if(m->GetValue() <= 0)
		throw std::invalid_argument("ALU::mul: argument 'mod' must be a positive.");

	//! Длина результирующего вектора не может превышать 32 бит
	const std::size_t mod = std::min<std::size_t>(m->GetValue(), 32);
	//! Для целочисленный констант
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();

		//! Битовая маска, ограничивающая результат
		unsigned mask = 0;
		for(unsigned j = 0; j < mod; ++j)
			mask |= (static_cast<unsigned>(1) << j);

		return boost::make_shared<Integer>((x_value * y_value) & mask);
	}
	//! Пропозициональный код
	return SemanticLib::multiply(toBitVector(x), toBitVector(y), mod, *encode_);
}

VoidPtr ALU::div(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::div: argument - null pointer.");

	//! Деление только для целочисленных переменных
	if(!(x->IsInt() && y->IsInt()))
		throw std::runtime_error("ALU::div: the operands must be an integer.");
	const int x_value = toInt(x)->GetValue();
	const int y_value = toInt(y)->GetValue();
	if(y_value == 0)
		throw std::runtime_error("ALU::div: division by zero.");
	return boost::make_shared<Integer>(x_value / y_value);
}

VoidPtr ALU::mod(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::mod: argument - null pointer.");

	//! Деление только для целочисленных переменных
	if(!(x->IsInt() && y->IsInt()))
		throw std::runtime_error("ALU::div: the operands must be an integer.");
	const int x_value = toInt(x)->GetValue();
	const int y_value = toInt(y)->GetValue();
	if(y_value == 0)
		throw std::runtime_error("ALU::div: division by zero.");
	return boost::make_shared<Integer>(x_value % y_value);
}

VoidPtr ALU::not_(VoidPtr x)
{
	if(!x)
		throw std::invalid_argument("ALU::not_: argument - null pointer.");

	//! Для целых чисел применяем по-битовое отрицание
	if(x->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		return boost::make_shared<Integer>(~x_value);
	}
	//! Пропозициональный код
	return SemanticLib::not_(toBitVector(x));
}

VoidPtr ALU::and_(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::and_: argument - null pointer.");

	//! Для целочисленный констант
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		return boost::make_shared<Integer>(x_value & y_value);
	}

	//! Пропозициональный код
	return SemanticLib::and_(toBitVector(x), toBitVector(y));
}

VoidPtr ALU::or_(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::or_: argument - null pointer.");

	//! Для целочисленный констант
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		return boost::make_shared<Integer>(x_value | y_value);
	}

	//! Пропозициональный код
	return SemanticLib::or_(toBitVector(x), toBitVector(y));
}

VoidPtr ALU::xor_(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::xor_: argument - null pointer.");

	//! Для целочисленный констант
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		return boost::make_shared<Integer>(x_value ^ y_value);
	}

	//! Пропозициональный код
	return SemanticLib::xor_(toBitVector(x), toBitVector(y));
}

VoidPtr ALU::concat(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::concat: argument - null pointer.");

	BitVectorPtr x_vec = toBitVector(x);
	BitVectorPtr y_vec = toBitVector(y);
	LogicFormulaContainer formulae;
	{
		for(unsigned i = 0, size = x_vec->GetSize(); i < size; ++i)
			formulae.push_back(x_vec->GetBit(i)->GetValue());
	}
	{
		for(unsigned i = 0, size = y_vec->GetSize(); i < size; ++i)
			formulae.push_back(y_vec->GetBit(i)->GetValue());
	}
	return boost::make_shared<BitVector>(formulae);
}

BitPtr ALU::great(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::great: argument - null pointer.");

	//! Сравнение констант - результат известен
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		const LogicFormulaPtr& value = (x_value > y_value ? EncodeFactory::True() : EncodeFactory::False());
		return boost::make_shared<Bit>(value);
	}

	//! Пропозициональное кодирование
	//! Сначала рассмотрим частные случае, когда один из аргументов - это целая константа
	if(x->IsBit() && y->IsConst())
	{
		LogicFormulaPtr value = SemanticLib::great_const(toBitVector(x), toBitVector(y), *encode_);
		return boost::make_shared<Bit>(value);
	}
	if(x->IsConst() && y->IsBit())
	{
		LogicFormulaPtr value = SemanticLib::less_const(toBitVector(y), toBitVector(x), *encode_);
		return boost::make_shared<Bit>(value);
	}

	LogicFormulaPtr value = SemanticLib::great(toBitVector(x), toBitVector(y), *encode_);
	return boost::make_shared<Bit>(value);
}

BitPtr ALU::ge(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::ge: argument - null pointer.");

	//! Сравнение констант - результат известен
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		const LogicFormulaPtr& value = (x_value >= y_value ? EncodeFactory::True() : EncodeFactory::False());
		return boost::make_shared<Bit>(value);
	}

	//! Пропозициональное кодирование
	//! Сначала рассмотрим частные случае, когда один из аргументов - это целая константа
	if(x->IsBit() && y->IsConst())
	{
		LogicFormulaPtr value = SemanticLib::ge_const(toBitVector(x), toBitVector(y), *encode_);
		return boost::make_shared<Bit>(value);
	}
	if(x->IsConst() && y->IsBit())
	{
		LogicFormulaPtr value = SemanticLib::le_const(toBitVector(y), toBitVector(x), *encode_);
		return boost::make_shared<Bit>(value);
	}

	LogicFormulaPtr value = SemanticLib::ge(toBitVector(x), toBitVector(y), *encode_);
	return boost::make_shared<Bit>(value);
}

BitPtr ALU::less(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::less: argument - null pointer.");

	return ALU::great(y, x);
}

BitPtr ALU::le(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::le: argument - null pointer.");

	return ALU::ge(y, x);
}

BitPtr ALU::equiv(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::equiv: argument - null pointer.");

	//! Сравнение констант - результат известен
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		const LogicFormulaPtr& value = (x_value == y_value ? EncodeFactory::True() : EncodeFactory::False());
		return boost::make_shared<Bit>(value);
	}

	//! Пропозициональное кодирование
	//! Сначала рассмотрим частные случае, когда один из аргументов - это целая константа
	if(x->IsBit() && y->IsConst())
	{
		LogicFormulaPtr value = SemanticLib::eq_const(toBitVector(x), toBitVector(y), *encode_);
		return boost::make_shared<Bit>(value);
	}
	if(x->IsConst() && y->IsBit())
	{
		LogicFormulaPtr value = SemanticLib::eq_const(toBitVector(y), toBitVector(x), *encode_);
		return boost::make_shared<Bit>(value);
	}

	LogicFormulaPtr value = SemanticLib::eq(toBitVector(x), toBitVector(y), *encode_);
	return boost::make_shared<Bit>(value);
}

BitPtr ALU::neq(VoidPtr x, VoidPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::neq: argument - null pointer.");

	//! Сравнение констант - результат известен
	if(x->IsInt() && y->IsInt())
	{
		const int x_value = toInt(x)->GetValue();
		const int y_value = toInt(y)->GetValue();
		const LogicFormulaPtr& value = (x_value != y_value ? EncodeFactory::True() : EncodeFactory::False());
		return boost::make_shared<Bit>(value);
	}

	//! Пропозициональное кодирование
	//! Сначала рассмотрим частные случае, когда один из аргументов - это целая константа
	if(x->IsBit() && y->IsConst())
	{
		LogicFormulaPtr value = SemanticLib::neq_const(toBitVector(x), toBitVector(y), *encode_);
		return boost::make_shared<Bit>(value);
	}
	if(x->IsConst() && y->IsBit())
	{
		LogicFormulaPtr value = SemanticLib::neq_const(toBitVector(y), toBitVector(x), *encode_);
		return boost::make_shared<Bit>(value);
	}

	LogicFormulaPtr value = SemanticLib::neq(toBitVector(x), toBitVector(y), *encode_);
	return boost::make_shared<Bit>(value);
}

VoidPtr ALU::LeftShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::LeftShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		const int value = toInt(x)->GetValue();
		return boost::make_shared<Integer>(value << shift_pos);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdLeft, BitVector::smZero);
	return result;
}

VoidPtr ALU::RightShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::RightShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		const int value = toInt(x)->GetValue();
		return boost::make_shared<Integer>(value >> shift_pos);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdRight, BitVector::smZero);
	return result;
}

VoidPtr ALU::LeftCircleShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::LeftCircleShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		int value = toInt(x)->GetValue();
		value = SemanticLib::CircleLeftShift(value, shift_pos);
		return boost::make_shared<Integer>(value);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdLeft, BitVector::smCircle);
	return result;
}

VoidPtr ALU::RightCircleShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::RightCircleShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		int value = toInt(x)->GetValue();
		value = SemanticLib::CircleRightShift(value, shift_pos);
		return boost::make_shared<Integer>(value);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdRight, BitVector::smCircle);
	return result;
}

VoidPtr ALU::LeftSaveShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::LeftSaveShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		int value = toInt(x)->GetValue();
		value = SemanticLib::SaveLeftShift(value, shift_pos);
		return boost::make_shared<Integer>(value);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdLeft, BitVector::smSave);
	return result;
}

VoidPtr ALU::RightSaveShift(VoidPtr x, IntegerPtr y)
{
	if(!x || !y)
		throw std::invalid_argument("ALU::RightSaveShift: argument - null pointer.");

	const int shift_pos = y->GetValue();

	if(shift_pos < 0)
		throw std::invalid_argument("ALU::LeftShift: shift position must be a nonnegative integer.");

	if(shift_pos == 0)
		return x;

	if(x->IsInt())
	{
		int value = toInt(x)->GetValue();
		value = SemanticLib::SaveRightShift(value, shift_pos);
		return boost::make_shared<Integer>(value);
	}

	//! Сдвиг применяем к независимой копии
	BitVectorPtr vec = toBitVector(x);
	BitVectorPtr result = boost::make_shared<BitVector>(*vec);
	result->Shift(shift_pos, BitVector::sdRight, BitVector::smSave);
	return result;
}

} // namespace Transalg
