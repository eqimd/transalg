#include "stdafx.h"
#include "../Interpreter.h"
#include "InterpreterImpl.h"

namespace Transalg
{

class Interpreter::Impl
{
public:

	InterpreterImpl instance_;
};

Interpreter::Interpreter()
{
	impl_ = boost::make_shared<Interpreter::Impl>();
}

bool Interpreter::TranslateProgram(const StatementBlockPtr& program, EncodeFactoryPtr encode)
{
	return impl_->instance_.TranslateProgram(program, encode);
}

} // namespace Transalg
