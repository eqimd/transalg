#pragma once

#include "DataTypeVoid.h"
#include <TransalgFormats/Types.h>

namespace Transalg
{

//! Тип данных Bit, представляет переменную ТА-программы типа bit
class Bit: public Void
{
public:

	Bit();
	Bit(LogicFormulaPtr data);
	Bit(const Bit&);

	// оператор присваивания
	Bit& operator=(const Bit& be);
	Bit& operator=(const Integer& be);

	friend bool operator==(const Bit& a, const Bit& b);
	friend std::ostream& operator<<(std::ostream& out, const Bit& a);

public:

	std::size_t GetSize() const {return 1;}

	bool IsConst() const;

	bool IsArray() const {return false;}
	
	std::string ToString() const;

	//! С переменной ТА-программы типа bit может быть связана произвольная формула над переменными кода
	LogicFormulaPtr GetValue() const {return data_;}
	
	//! Связать с переменной программы переменную кода
	void SetValue(LogicFormulaPtr value) {data_ = value;}

private:
	
	LogicFormulaPtr data_;
};

//! Битовый вектор - организует работу с набором переменных типа Bit как с единым целым
class BitVector: public Void
{
public:

	BitVector(std::size_t size);
	BitVector(const std::string& hex_string);
	BitVector(const LogicFormulaContainer& data);
	BitVector(const BitContainer& data);
	BitVector(const BitVector&);

	BitVector& operator=(const BitVector& be);
	BitVector& operator=(const Integer& be);

	friend bool operator==(const BitVector& a, const BitVector& b);
	friend std::ostream& operator<<(std::ostream& out, const BitVector& a);

public:

	// Void virtual interface

	std::size_t GetSize() const;

	bool IsConst() const;

	bool IsArray() const;

	std::string ToString() const;

public:

	typedef enum {sdLeft, sdRight} ShiftDirection;
	typedef enum {smZero, smSave, smCircle} ShiftMode;

	const BitPtr&  operator[](unsigned index) const;
	BitPtr&        operator[](unsigned index);

	BitPtr GetBit(unsigned index = 0) const;
	
	void SetBit(BitPtr value, unsigned index = 0);

	//! Старший значаший бит (актуально, если есть ведущие нули)
	const BitPtr& GetMajorBit() const;

	void PushBack(const BitPtr& bit);
	void PushBack(const LogicFormulaPtr& bit);

	//! Старший бит
	const BitPtr& Back() const;
	BitPtr& Back();

	//! Младший бит
	const BitPtr& Front() const;
	BitPtr& Front();

	void Shift(unsigned pos, ShiftDirection direction = sdLeft, ShiftMode mode = smZero);

	void Resize(std::size_t size);

	void RemoveLeadZero();

private:

	//! Кодированный битовый вектор
	//! data_.front() - младший бит вектора
	//! data_.back() - старший бит вектора
	BitContainer data_;
};

} // namespace Transalg
