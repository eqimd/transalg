#include "stdafx.h"
#include "InterpreterImpl.h"
#include "ALU.h"
#include "DataTypeBit.h"
#include "DataTypeInteger.h"

namespace Transalg
{

bool InterpreterImpl::TranslateProgram(const StatementBlockPtr& program, EncodeFactoryPtr encode)
{
	try
	{
		if(!program)
			throw std::invalid_argument("Interpreter::TranslateProgram: program - null pointer.");
		if(!encode)
			throw std::invalid_argument("Interpreter::TranslateProgram: encode - null pointer.");

		encode_ = encode;
		//! Создаем новый экземляр вычислителя выражения
		alu_.reset(new ALU(this, encode));
		//! Список глобальных объектов: глобальные переменные/массивы и все функции ТА-программы
		context_.push(boost::make_shared<DataObjectContext>());
		for(StatementContainer::const_iterator stmt_it = program->begin(); stmt_it != program->end(); ++stmt_it)
		{
			if(StatementVarDefinitionPtr var_def = boost::dynamic_pointer_cast<StatementVarDefinition>(*stmt_it))
			{
				context_.top()->AddObject(ExecStatementVarDefinition(var_def));
			}
			else if(StatementArrayDefinitionPtr array_def = boost::dynamic_pointer_cast<StatementArrayDefinition>(*stmt_it))
			{
				context_.top()->AddObject(ExecStatementArrayDefinition(array_def));
			}
			else if(StatementFunctionDefinitionPtr func_def = boost::dynamic_pointer_cast<StatementFunctionDefinition>(*stmt_it))
			{
				context_.top()->AddObject(ExecStatementFunctionDefinition(func_def));
			}
			else
				throw std::runtime_error("InterpreterImpl::TranslateProgram(): incorrect type of definition statement.");
		}

		//! Ищем функцию main и начинаем интерпретацию с нее
		const std::string main_func("main");
		DataObjectPtr obj = context_.top()->FindObject(main_func);
		if(!obj)
			throw std::runtime_error("InterpreterImpl::TranslateProgram(): the main function not found.");
		DataObjectFunctionPtr func_obj = boost::dynamic_pointer_cast<DataObjectFunction>(obj);
		if(!func_obj)
			throw std::runtime_error("InterpreterImpl::TranslateProgram(): the object with 'main' id must be a function.");

		//! Начинаем интерпретацию
		VoidContainer arguments;
		ExecFunctionCall(func_obj, arguments);
	}
	catch(const std::runtime_error& e)
	{
		std::stringstream msg;
		msg << e.what() << std::endl;
		//! Добавим описание контекста, в котором обнаружена ошибка
		// to do

		//! Передаем сообщение ошибке дальше наверх
		// fixme: throw e;
		throw std::runtime_error(msg.str());
	}

	return true;
}

void InterpreterImpl::ExecStatement(const StatementPtr& stmt, VoidPtr& func_result)
{
	//! func_result - значение текущей функции
	//! Если значение текущей функции уже вычислено, то ничего не делаем
	if(func_result)
		return;

	switch(stmt->GetStatementType())
	{
	case stmtVarDefinition:
		{
			StatementVarDefinitionPtr var_stmt = boost::dynamic_pointer_cast<StatementVarDefinition>(stmt);
			if(!var_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementVarDefinition).");
			DataObjectVarPtr var_object = ExecStatementVarDefinition(var_stmt);
			context_.top()->AddObject(var_object);
		}
		break;
	case stmtArrayDefinition:
		{
			StatementArrayDefinitionPtr array_stmt = boost::dynamic_pointer_cast<StatementArrayDefinition>(stmt);
			if(!array_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementArrayDefinitioin).");
			DataObjectArrayPtr array_object = ExecStatementArrayDefinition(array_stmt);
			context_.top()->AddObject(array_object);
		}
		break;
	case stmtFunctionDefinition:
		{
			StatementFunctionDefinitionPtr func_stmt = boost::dynamic_pointer_cast<StatementFunctionDefinition>(stmt);
			if(!func_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementFunctionDefinition).");
			DataObjectFunctionPtr func_object = ExecStatementFunctionDefinition(func_stmt);
			context_.top()->AddObject(func_object);
		}
		break;
	case stmtExpression:
		{
			ExpressionPtr expression = boost::dynamic_pointer_cast<Expression>(stmt);
			if(!expression)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (Expression).");
			alu_->ExecuteExpression(expression, context_.top().get());
		}
		break;
	case stmtIf:
		{
			StatementIfPtr if_stmt = boost::dynamic_pointer_cast<StatementIf>(stmt);
			if(!if_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementIf).");
			ExecStatementIf(if_stmt, func_result);
		}
		break;
	case stmtFor:
		{
			StatementForPtr for_stmt = boost::dynamic_pointer_cast<StatementFor>(stmt);
			if(!for_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementFor).");
			ExecStatementFor(for_stmt, func_result);
		}
		break;
	case stmtReturn:
		{
			StatementReturnPtr return_stmt = boost::dynamic_pointer_cast<StatementReturn>(stmt);
			if(!return_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementReturn).");
			func_result = alu_->ExecuteExpression(return_stmt->GetReturnExpression(), context_.top().get());
		}
		break;
	case stmtBlock:
		{
			StatementBlockPtr block_stmt = boost::dynamic_pointer_cast<StatementBlock>(stmt);
			if(!block_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementBlock).");
			ExecStatementBlock(block_stmt, func_result);
		}
		break;
	case stmtTable:
		{
			StatementTablePtr table_stmt = boost::dynamic_pointer_cast<StatementTable>(stmt);
			if(!table_stmt)
				throw std::runtime_error("InterpreterImpl::ExecStatement(): bad dynamic type cast (StatementTable).");
			ExecStatementTable(table_stmt);
		}
		break;
	default:
		throw std::runtime_error("InterpreterImpl::ExecStatement(): unknown type of statement.");
	}
}

void InterpreterImpl::ExecStatementBlock(const StatementBlockPtr& block, VoidPtr& func_result)
{
	//! Создаем новый контекст
	const DataObjectContext* current_context = context_.top().get();
	context_.push(boost::make_shared<DataObjectContext>(current_context));
	//! Выполняем инструкции блока
	for(StatementContainer::const_iterator stmt_it = block->begin(); stmt_it != block->end(); ++stmt_it)
	{
		ExecStatement(*stmt_it, func_result);
		//! func_result содержит результат интерпретации инструкции return для текущей функции
		if(func_result) break;
	}
	//! Возвращаемся к родительскому контексту (объекты текущего уничтожаются)
	context_.pop();
}

DataObjectVarPtr InterpreterImpl::ExecStatementVarDefinition(const StatementVarDefinitionPtr& stmt)
{
	assert(stmt);
	DataObjectVarPtr var_object = 
		boost::make_shared<DataObjectVar>(stmt->GetAttribute(), stmt->GetDataType(), stmt->GetId());

	if(BitPtr var = boost::dynamic_pointer_cast<Bit>(var_object->data))
	{
		assert(stmt->GetDataType() == Transalg::dtBit);

		//! Проверяем наличие атрибутов, и если они есть, то создаем новые переменные кода
		LogicVarPtr encode_var;
		if(stmt->GetAttribute() == Transalg::AttributeIn)
		{
			encode_var = encode_->CreateVar(Transalg::LogicVarInput);
		}
		else if(stmt->GetAttribute() == Transalg::AttributeOut)
		{
			encode_var = encode_->CreateVar(Transalg::LogicVarOutput);
		}
		else if(stmt->GetAttribute() == Transalg::AttributeMem)
		{
			encode_var = encode_->CreateVar(Transalg::LogicVarTseitin);
		}

		//! Есть инициализирующее выражение
		if(stmt->GetInitValue())
		{
			BitPtr init_value = toBit(alu_->ExecuteExpression(stmt->GetInitValue(), context_.top().get()));
			if(encode_var)
			{
				//! Инициализируем значение переменной формулой, а саму переменную связываем с переменной программы var
				encode_var->var_value = init_value->GetValue();
				var->SetValue(boost::make_shared<LogicFormula>(encode_var));
			}
			else
				//! С переменной программы связываем формулу
				var->SetValue(init_value->GetValue());
		}
		//! Создана переменная кода
		else if(encode_var)
		{
			var->SetValue(boost::make_shared<LogicFormula>(encode_var));
		}
		
		//! Добавим эту переменную программы в список локальных для текущего оператора ветвления
		if(IsBranchMode())
		{
			branchs_.top().local_vars.push_back(var);
		}
	}
	else if(IntegerPtr var = boost::dynamic_pointer_cast<Integer>(var_object->data))
	{
		assert(stmt->GetDataType() == Transalg::dtInt);
		if(stmt->GetInitValue())
		{
			IntegerPtr init_value = toInt(alu_->ExecuteExpression(stmt->GetInitValue(), context_.top().get()));
			var->SetValue(init_value->GetValue());
		}
	}
	else
	{
		std::stringstream msg;
		msg << "InterpreterImpl::ExecStatementVarDefinition(): bad dynamic type cast. Line: " << boost::lexical_cast<std::string>(stmt->GetLine()) << std::endl;
		msg << "DataObjectVar: attribute = " << var_object->GetAttribute() << ", data type = " << var_object->GetDataType() << ", id = " << var_object->GetObjectId() << std::endl;
		throw std::runtime_error(msg.str());
	}

	return var_object;
}

DataObjectArrayPtr InterpreterImpl::ExecStatementArrayDefinition(const StatementArrayDefinitionPtr& stmt)
{
	assert(stmt);
	//! Вычисляем размер массива
	std::vector<unsigned> indexes;
	for(unsigned i = 0; i < stmt->GetIndexCount(); ++i)
	{
		indexes.push_back(stmt->GetIndex(i));
	}

	//! Создаем объект массива
	DataObjectArrayPtr array_object = boost::make_shared<DataObjectArray>(stmt->GetAttribute(), stmt->GetDataType(), stmt->GetId(), indexes);
	const std::size_t array_size = array_object->GetSize();

	if(stmt->GetDataType() == Transalg::dtBit)
	{
		//! Проверяем наличие инициализации массива
		BitVectorPtr init_vector;
		//! Инициализация выражением
		if(stmt->GetInitExpression())
		{
			VoidPtr res = alu_->ExecuteExpression(stmt->GetInitExpression(), context_.top().get());
			init_vector = toBitVector(res);
			init_vector->Resize(array_size);
		}
		//! Инициализация таблицей
		else if(stmt->GetInitValueCount())
		{
			const std::size_t init_size = stmt->GetInitValueCount();
			if(array_size != init_size)
				throw std::runtime_error("InterpreterImpl::ExecStatementArrayDefinition(): size of array is not equal size of initial table.");
			if(init_size == 0)
				throw std::runtime_error("InterpreterImpl::ExecStatementArrayDefinition(): initial size == 0.");

			init_vector = boost::make_shared<BitVector>(0);

			for(unsigned i = 0; i < init_size; ++i)
			{
				const unsigned value = stmt->GetInitValue(i);
				BitPtr bit = boost::make_shared<Bit>(value == 0 ? EncodeFactory::False() : EncodeFactory::True());
				init_vector->PushBack(bit);
			}
		}

		//! Проверяем наличие атрибутов: если они есть, то создаем новые переменные кода
		std::vector<LogicVarPtr> encode_vector;
		if(stmt->GetAttribute() == Transalg::AttributeIn)
		{
			for(unsigned i = 0; i < array_size; ++i)
			{
				LogicVarPtr encode_var = encode_->CreateVar(Transalg::LogicVarInput);
				encode_vector.push_back(encode_var);
			}
		}
		else if(stmt->GetAttribute() == Transalg::AttributeOut)
		{
			for(unsigned i = 0; i < array_size; ++i)
			{
				LogicVarPtr encode_var = encode_->CreateVar(Transalg::LogicVarOutput);
				encode_vector.push_back(encode_var);
			}
		}
		else if(stmt->GetAttribute() == Transalg::AttributeMem)
		{
			for(unsigned i = 0; i < array_size; ++i)
			{
				LogicVarPtr encode_var = encode_->CreateVar(Transalg::LogicVarTseitin);
				encode_vector.push_back(encode_var);
			}
		}

		BitVectorPtr data = array_object->GetValue();
		//! Есть инициализирующее выражение
		if(init_vector)
		{
			assert(init_vector->GetSize() == array_size);
			const bool is_encode = (encode_vector.size() == array_size);
			for(unsigned i = 0; i < array_size; ++i)
			{
				LogicFormulaPtr iv = (*init_vector)[i]->GetValue();
				if(is_encode)
				{
					encode_vector[i]->var_value = iv;
					(*data)[i]->SetValue(boost::make_shared<LogicFormula>(encode_vector[i]));
				}
				else
					(*data)[i]->SetValue(iv);
			}
		}
		//! Созданы новые переменные кода
		else if(encode_vector.size() == array_size)
		{
			for(unsigned i = 0; i < array_size; ++i)
			{
				(*data)[i]->SetValue(boost::make_shared<LogicFormula>(encode_vector[i]));
			}
		}

		//! Добавим созданные переменные программы в список локальных переменных для текущего оператора ветвления (если он есть)
		if(IsBranchMode())
		{
			for(unsigned i = 0; i < data->GetSize(); ++i)
				branchs_.top().local_vars.push_back(data->GetBit(i));
		}
	}
	else if(stmt->GetDataType() == Transalg::dtInt)
	{
		//! Для целочисленных массивов допускается только табличная инициализация
		if(stmt->GetInitValueCount())
		{
			const std::size_t init_size = stmt->GetInitValueCount();
			if(array_size != init_size)
				throw std::runtime_error("InterpreterImpl::ExecStatementArrayDefinition(): size of array is not equal size of initial table.");

			for(unsigned i = 0; i < array_size; ++i)
			{
				IntegerPtr var = toInt((*array_object)[i]);
				const unsigned value = stmt->GetInitValue(i);
				var->SetValue(static_cast<int>(value));
			}
		}
	}
	else
		throw std::runtime_error("InterpreterImpl::ExecStatementArrayDefinition(): unknown data type of the array.");
	
	return array_object;
}

DataObjectFunctionPtr InterpreterImpl::ExecStatementFunctionDefinition(const StatementFunctionDefinitionPtr& stmt)
{
	return boost::make_shared<DataObjectFunction>(stmt);
}

void InterpreterImpl::ExecStatementIf(const StatementIfPtr& stmt, VoidPtr& func_result)
{
	VoidPtr cond = alu_->ExecuteExpression(stmt->GetCondition(), context_.top().get());
	if(!cond)
		throw std::runtime_error("InterpreterImpl::ExecStatementIf: condition - null pointer.");

	if(cond->IsConst())
	{
		//! Транслируем только одну ветку, т.к. значение предиката точно известно
		const int cond_value = toInt(cond)->GetValue();
		if(cond_value)
		{
			//! true-ветка должна существовать всегда
			if(!stmt->GetTrueBranch())
				throw std::runtime_error("InterpreterImpl::ExecStatementIf: true-branch does not exist.");
			ExecStatement(stmt->GetTrueBranch(), func_result);
		}
		//! false-ветка опциональна (может отсутствовать)
		else if(stmt->GetFalseBranch())
		{
			ExecStatement(stmt->GetFalseBranch(), func_result);
		}
		return;
	}

	branchs_.push(BranchMatrix());
	SuggestBranchs& sb = branchs_.top().suggest_branchs;
	//! Предикат условного оператора - некоторая булева формула
	BitPtr cond_formula = toBit(cond);
	sb.condition = cond_formula->GetValue();
	//! Интерпретация true-ветви
	sb.current_branch = &sb.suggest_true_branch;
	ExecStatement(stmt->GetTrueBranch(), func_result);
	//! Интерпретация false-ветви, если она есть
	if(stmt->GetFalseBranch())
	{
		sb.current_branch = &sb.suggest_false_branch;
		ExecStatement(stmt->GetFalseBranch(), func_result);
	}

	//! Построим пропозициональную кодировку условного оператора
	//! Формируем единый список переменных программы, которые могут изменяться при ветвлении
	SuggestContainer suggest_vars;
	ExtractUniqueVars(sb.suggest_true_branch, suggest_vars);
	ExtractUniqueVars(sb.suggest_false_branch, suggest_vars);
	for(SuggestContainer::iterator var_it = suggest_vars.begin(); var_it != suggest_vars.end(); ++var_it)
	{
		var_it->second.reset();
		//! Создаем новую переменную кода для предиката условного оператора
		if(sb.condition->IsFunction())
		{
			LogicVarPtr var_enc = encode_->CreateVar(sb.condition);
			sb.condition = boost::make_shared<LogicFormula>(var_enc);
		}
		//! Проверим наличие данной переменной программы в true-ветви
		{
			SuggestContainer::const_iterator branch_var_it = FindVarInSuggest(var_it->first, sb.suggest_true_branch);
			//! Значение данной переменной программы в true-ветви
			LogicFormulaPtr var_value = (branch_var_it != sb.suggest_true_branch.end() ? 
				branch_var_it->second : var_it->first->GetValue());

			var_it->second = EncodeFactory::Or(var_it->second, EncodeFactory::And(sb.condition, var_value));
		}
		//! Проверим наличие данной переменной в false-ветви
		{
			SuggestContainer::const_iterator branch_var_it = FindVarInSuggest(var_it->first, sb.suggest_false_branch);
			//! Значение данной переменной программы в false-ветви
			LogicFormulaPtr var_value = (branch_var_it != sb.suggest_false_branch.end() ? 
				branch_var_it->second : var_it->first->GetValue());

			var_it->second = EncodeFactory::Or(var_it->second, EncodeFactory::And(EncodeFactory::Not(sb.condition), var_value));
		}
	}

	//! Создаем новые переменные кода
	for(SuggestContainer::iterator var_it = suggest_vars.begin(); var_it != suggest_vars.end(); ++var_it)
	{
		LogicVarPtr var_enc = encode_->CreateVar(var_it->second);
		var_it->first->SetValue(boost::make_shared<LogicFormula>(var_enc));
	}

	branchs_.pop();
}

void InterpreterImpl::ExtractUniqueVars(const SuggestContainer& source, SuggestContainer& target) const
{
	for(unsigned i = 0; i < source.size(); ++i)
	{
		const Suggest& var_suggest = source[i];
		SuggestContainer::const_iterator var_it = FindVarInSuggest(var_suggest.first, target);
		//! Если такая переменная программы еще не втречалась, добавляем ее в target
		if(var_it == target.end())
		{
			target.push_back(Suggest(var_suggest.first, LogicFormulaPtr()));
		}
	}
}

SuggestContainer::const_iterator 
InterpreterImpl::FindVarInSuggest(const BitPtr& var, const SuggestContainer& var_container) const
{
	for(SuggestContainer::const_iterator it = var_container.begin();
		it != var_container.end();
		++it)
	{
		if(it->first == var)
			return it;
	}
	return var_container.end();
}

SuggestContainer::iterator 
InterpreterImpl::FindVarInSuggest(const BitPtr& var, SuggestContainer& var_container) const
{
	for(SuggestContainer::iterator it = var_container.begin();
		it != var_container.end();
		++it)
	{
		if(it->first == var)
			return it;
	}
	return var_container.end();
}

bool InterpreterImpl::IsLocalVar(const BitPtr& var) const
{
	if(branchs_.empty())
		return false;
	const BitContainer& local_vars = branchs_.top().local_vars;
	BitContainer::const_iterator it = std::find(local_vars.begin(), local_vars.end(), var);
	return it != local_vars.end();
}

bool InterpreterImpl::IsBranchMode() const
{
	return !branchs_.empty();
}

SuggestContainer* InterpreterImpl::GetSuggestBranchVars() const
{
	if(branchs_.empty())
		throw std::runtime_error("InterpreterImpl::GetSuggestBranchVars(): ");

	return branchs_.top().suggest_branchs.current_branch;
}

void InterpreterImpl::ExecStatementFor(const StatementForPtr& stmt, VoidPtr& func_result)
{
	//! Выполняем шаг инициалазации (объявление переменной и/или вычисление выражения)
	ExecStatement(stmt->GetInitStatement(), func_result);
	//! Вычисляем условие цпкла
	while( toInt(alu_->ExecuteExpression(stmt->GetConditionExpression(), context_.top().get()))->GetValue() )
	{
		//! Выполнить тело цикла
		ExecStatement(stmt->GetBody(), func_result);
		if(func_result) break;
		//! Шаг цикла
		alu_->ExecuteExpression(stmt->GetStepExpression(), context_.top().get());
	}
}

VoidPtr InterpreterImpl::ExecFunctionCall(const DataObjectFunctionPtr& func_ptr, const VoidContainer& arguments)
{
	//! Число фактических и формальных параметров совпадает
	if(func_ptr->GetArgCount() != arguments.size())
	{
		std::stringstream msg;
		msg << "InterpreterImpl::ExecFunctionCall(): the '" << func_ptr->GetObjectId() << "' does not takes " 
			<< arguments.size() << (arguments.size() == 1 ? "argument" : " argument's");
		throw std::runtime_error(msg.str());
	}

	//! Создаем новый контекст и заполняем его объектами данных
	const DataObjectContext* current_context = context_.top().get();
	context_.push(boost::make_shared<DataObjectContext>(current_context));

	//! Вычисляем аргументы функции
	for(unsigned arg_num = 0; arg_num < arguments.size(); ++arg_num)
	{
		//! Параметры функций не могут иметь атрибуты
		const DataType arg_data_type = func_ptr->GetArgDataType(arg_num);
		const std::string& arg_id = func_ptr->GetArgId(arg_num);
		const std::vector<unsigned>& arg_indexes = func_ptr->GetArgIndexes(arg_num);

		//! Аргумент функции - это переменная
		if(arg_indexes.empty())
		{
			VoidPtr init_value;
			if(arg_data_type == Transalg::dtInt)
				init_value = toInt(arguments[arg_num]);
			else if(arg_data_type == Transalg::dtBit)
				init_value = toBit(arguments[arg_num]);
			else
			{
				std::stringstream msg;
				msg << "InterpreterImpl::ExecFunctionCall(): invalid type of function argument #" << arg_num;
				throw std::runtime_error(msg.str());
			}

			DataObjectVarPtr var_object = boost::make_shared<DataObjectVar>(Transalg::AttributeNone, 
				arg_data_type, arg_id, init_value);
			context_.top()->AddObject(var_object);
		}
		//! Аргумент функции - это массив
		else
		{
			DataObjectArrayPtr array_object = boost::make_shared<DataObjectArray>(Transalg::AttributeNone, 
				arg_data_type, arg_id, arg_indexes);

			//! Инициализация элементов массива переданным функции аргументом
			if(arg_data_type == Transalg::dtBit)
			{
				BitVectorPtr init_value = toBitVector(arguments[arg_num]);
				BitVectorPtr array_bits = array_object->GetValue();
				const std::size_t size = std::min(array_bits->GetSize(), init_value->GetSize());
				for(unsigned i = 0; i < size; ++i)
				{
					(*array_bits)[i]->SetValue((*init_value)[i]->GetValue());
				}
				for(unsigned i = size; i < array_bits->GetSize(); ++i)
				{
					(*array_bits)[i]->SetValue(EncodeFactory::False());
				}
			}
			else
			{
				//! fixme: нужно проработать этот момент
				//! to do: передача целочисленных массивов как аргументов функций еще не реализована
				std::stringstream msg;
				msg << "InterpreterImpl::ExecFunctionCall(): invalid type of function argument #" << arg_num;
				throw std::runtime_error(msg.str());
			}
			context_.top()->AddObject(array_object);
		}

	}
	//! Выполняем инструкции
	VoidPtr func_result;
	StatementBlockPtr block = func_ptr->GetBody();
	for(StatementContainer::const_iterator stmt_it = block->begin(); stmt_it != block->end(); ++stmt_it)
	{
		ExecStatement(*stmt_it, func_result);
		//! func_result содержит результат интерпретации инструкции return
		if(func_result) break;
	}

	//! Вернемся к родительскому контексту
	context_.pop();

	return func_result;
}

void InterpreterImpl::ExecStatementTable(const StatementTablePtr& table)
{}

} // namespace Transalg
