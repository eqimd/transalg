#include "stdafx.h"
#include "DataTypeBit.h"
#include "DataTypeInteger.h"
#include "SemanticLibrary.h"
#include <TransalgFormats/EncodeFactory.h>

namespace Transalg
{

Bit::Bit()
	: Void(Transalg::dtBit)
	, data_(EncodeFactory::False())
{
}

Bit::Bit(LogicFormulaPtr formula)
	: Void(Transalg::dtBit)
	, data_(formula)
{
	assert(data_);
}

Bit::Bit(const Bit& b)
	: Void(Transalg::dtBit)
	, data_(b.data_)
{
	assert(data_);
}

Bit& Bit::operator=(const Bit& be)
{
	// проверка на самоприсваивание
	if(this == &be) return *this;
	data_ = be.GetValue();
	return *this;
}

Bit& Bit::operator=(const Integer& be)
{
	data_ = (be.GetValue() == 0 ? EncodeFactory::False() : EncodeFactory::True());
	return *this;
}

bool Bit::IsConst() const 
{
	return data_->IsConst();
}

std::string Bit::ToString() const
{
	std::stringstream out;
	out << "[Bit]: " << *data_;
	return out.str();
}

////////////////////////////////////////////////
// BitVector

BitVector::BitVector(std::size_t size)
	: Void(Transalg::dtBit)
{
	for(unsigned i = 0; i < size; ++i)
		data_.push_back(boost::make_shared<Bit>());
}

BitVector::BitVector(const LogicFormulaContainer& data)
	: Void(Transalg::dtBit)
{
	for(unsigned i = 0; i < data.size(); ++i)
		data_.push_back(boost::make_shared<Bit>(data[i]));
}

BitVector::BitVector(const BitContainer& data)
	: Void(Transalg::dtBit)
	, data_(data)
{
}

BitVector::BitVector(const BitVector& vec)
	: Void(Transalg::dtBit)
	, data_(vec.data_)
{
}

BitVector::BitVector(const std::string& hex_string)
	: Void(Transalg::dtBit)
{
	if(hex_string.size() < 3 || hex_string.substr(0, 2) != "0x")
		throw std::invalid_argument("BitVector::BitVector(const std::string& hex_string): invalid hex-string.");

	std::string data(hex_string.substr(2));
	while(!data.empty())
	{
		const std::size_t pos = (data.size() >= 8 ? data.size() - 8 : 0);
		const std::string hex_word(data.substr(pos));
		unsigned value;
		{
			std::stringstream ss;
			ss << std::hex << hex_word;
			ss >> value;
		}
		data.resize(data.size() - hex_word.size());
		if(data.empty())
		{
			while(value)
			{
				const LogicFormulaPtr bit = (value & 1 ? EncodeFactory::True() : EncodeFactory::False());
				data_.push_back(boost::make_shared<Bit>(bit));
				value >>= 1;
			}
		}
		else
		{
			for(unsigned i(0); i < 32; ++i)
			{
				const LogicFormulaPtr bit = (value & 1 ? EncodeFactory::True() : EncodeFactory::False());
				data_.push_back(boost::make_shared<Bit>(bit));
				value >>= 1;
			}
		}
	}

	if(data_.size() == 0)
		data_.push_back(boost::make_shared<Bit>());
}

BitVector& BitVector::operator=(const BitVector& be)
{
	if(this == &be) return *this;
	const std::size_t min_size = std::min(data_.size(), be.GetSize());
	unsigned i(0);
	for(; i < min_size; ++i)
	{
		data_[i]->SetValue(be[i]->GetValue());
	}
	//! Оставшиеся позиции заполняем нулями
	for(i = min_size; i < data_.size(); ++i)
	{
		data_[i]->SetValue(EncodeFactory::False());
	}
	return *this;
}

BitVector& BitVector::operator=(const Integer& be)
{
	const int rvalue = be.GetValue();
	const std::size_t rvalue_size = 8 * sizeof(rvalue);
	const std::size_t min_size = std::min(data_.size(), rvalue_size);
	unsigned i(0);
	for(; i < min_size; ++i)
	{
		const bool bit = (rvalue & (1 << i)) != 0;
		data_[i]->SetValue(bit ? EncodeFactory::True() : EncodeFactory::False());
	}
	//! Оставшиеся позиции заполняем нулями
	for(i = min_size; i < data_.size(); ++i)
	{
		data_[i]->SetValue(EncodeFactory::False());
	}
	return *this;
}

std::size_t BitVector::GetSize() const
{
	return data_.size();
}

bool BitVector::IsConst() const
{
	for(unsigned i = 0; i < data_.size(); ++i)
	{
		const LogicFormulaPtr& value = data_[i]->GetValue();
		if(!value->IsConst() || (value->IsConst() && value->back().value.Undef()))
		{
			return false;
		}
	}
	return true;
}

bool BitVector::IsArray() const
{
	return true;
}

const BitPtr& BitVector::operator[](unsigned index) const 
{
	return data_.at(index);
}

BitPtr& BitVector::operator[](unsigned index)
{
	return data_.at(index);
}

BitPtr BitVector::GetBit(unsigned index) const
{
	return data_.at(index);
}

void BitVector::SetBit(BitPtr value, unsigned index)
{
	if(index >= data_.size())
	{
		throw std::runtime_error("BitVector::SetValue: index out of range!");
	}
	data_[index] = value;
}

const BitPtr& BitVector::GetMajorBit() const
{
	assert(data_.size());
	for(BitContainer::const_reverse_iterator it = data_.rbegin(); it != data_.rend(); ++it)
	{
		const LogicFormulaPtr& value = (*it)->GetValue();
		if(value->IsConst())
		{
			const LogicValueType v(value->back().value);
			if(v.True() || v.Undef())
				return *it;
		}
	}
	//! Младший бит, даже если он равен 0
	return data_.front();
}

void BitVector::PushBack(const BitPtr& bit)
{
	data_.push_back(bit);
}

void BitVector::PushBack(const LogicFormulaPtr& bit)
{
	data_.push_back(boost::make_shared<Bit>(bit));
}

const BitPtr& BitVector::Back() const
{
	assert(data_.size());
	return data_.back();
}

BitPtr& BitVector::Back()
{
	assert(data_.size());
	return data_.back();
}

const BitPtr& BitVector::Front() const
{
	assert(data_.size());
	return data_.front();
}

BitPtr& BitVector::Front()
{
	assert(data_.size());
	return data_.front();
}

void BitVector::Shift(unsigned pos, BitVector::ShiftDirection direction, BitVector::ShiftMode mode)
{
	assert(data_.size());
	unsigned i;
	const unsigned size = data_.size();
	pos = pos % size;
	// запоминаем биты, которые "выталкиваются" из вектора
	BitContainer pushed(pos);

	//! Сдвиг влево - в сторону старшего бита
	if(direction == sdLeft)
	{
		for(i = 0; i < pos; ++i)
		{
			pushed[i] = data_[size - pos + i];
		}
		switch(mode)
		{
			case smZero:

				for(i = size; i > 0; --i)
				{
					data_[i - 1] = ( i >= pos + 1 ? data_[i - pos - 1] : boost::make_shared<Bit>());
				}
				
				break;
			case smSave:

				for(i = size; i > pos; --i)
					data_[i - 1] = data_[i - 1 - pos];

				break;
			case smCircle:

				for(i = size; i > pos; --i)
					data_[i - 1] = data_[i - 1 - pos];
				
				for(i = pos; i > 0; --i)
					data_[i - 1] = pushed[i - 1];
				
				break;
			default: break;
		}
	}
	//! Сдвиг вправо - в сторону младшего бита
	else
	{
		for(i = 0; i < pos; ++i)
		{
			pushed[i] = data_[pos - i - 1];
		}
		switch(mode)
		{
			case smZero:

				for(i = 0; i < size; ++i)
					data_[i] = ( i < size - pos ? data_[i + pos] : boost::make_shared<Bit>());

				break;
			case smSave:

				for(i = 0; i < size - pos; ++i)
					data_[i] = data_[i + pos];
				
				break;
			case smCircle:

				for(i = 0; i < size - pos; ++i)
					data_[i] = data_[i + pos];

				for(i = 0; i < pos; ++i)
					data_[size - i - 1] = pushed[i];

				break;
		}
	}
}

void BitVector::Resize(std::size_t size)
{
	if(size > data_.size())
	{
		for(unsigned i = data_.size(); i < size; ++i)
			data_.push_back(boost::make_shared<Bit>());
		return;
	}

	data_.resize(size);
}

void BitVector::RemoveLeadZero()
{
	assert(data_.size());
	std::size_t size = data_.size();
	//! Последний бит всегда оставляем, даже если он нулевой
	for(; size > 1; --size)
	{
		const LogicFormulaPtr& value = data_[size - 1]->GetValue();
		//! Завершаем цикл как только встречаем бит, отличный от нуля
		if(value && !(value->IsConst() && value->back().value.False()))
		{
			break;
		}
	}
	Resize(size);
}

std::string BitVector::ToString() const
{
	std::stringstream ss;
	ss << "[Vector]:\n";
	if(IsConst())
	{
		for(unsigned i = 0; i < data_.size();)
		{
			const LogicFormulaPtr& f = data_[i]->GetValue();
			ss << (f->back().value.True() ? "1" : "0");
			ss << (++i == data_.size() ? "\n" : " ");
		}
	}
	else
	{
		for(unsigned i = 0; i < data_.size(); ++i)
		{
			ss << "\t" << data_[i]->ToString() << "\n";
		}
	}
	return ss.str();
}

bool operator==(const Bit& a, const Bit& b)
{
	if(!a.data_ || !b.data_)
		return false;

	return *a.data_ == *b.data_;
}

bool operator==(const BitVector& a, const BitVector& b)
{
	if(a.data_.size() != b.data_.size())
		return false;

	const std::size_t size(a.data_.size());
	for(unsigned i = 0; i < size; ++i)
	{
		const Bit& ai = *(a[i]);
		const Bit& bi = *(b[i]);
		if(!(ai == bi))
			return false;
	}
	return true;
}

std::ostream& operator<<(std::ostream& out, const Bit& a)
{
	out << *(a.data_);
	return out;
}

std::ostream& operator<<(std::ostream& out, const BitVector& a)
{
	for(unsigned i = a.data_.size(); i > 0; --i)
	{
		const Bit& b = *(a[i - 1]);
		out << b;
	}
	return out;
}

} // namespace Transalg
