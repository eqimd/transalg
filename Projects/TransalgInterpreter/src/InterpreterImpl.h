#pragma once

#include <TransalgFormats/EncodeFactory.h>
#include "IInterpreterImpl.h"
#include "IALU.h"

namespace Transalg
{

class InterpreterImpl: public IInterpreterImpl
{
public:

	InterpreterImpl() {}

	//! Пропозициональное кодирование ТА-программы
	bool TranslateProgram(const StatementBlockPtr& program, EncodeFactoryPtr encode);

public:

	DataObjectVarPtr ExecStatementVarDefinition(const StatementVarDefinitionPtr& stmt);

	DataObjectArrayPtr ExecStatementArrayDefinition(const StatementArrayDefinitionPtr& stmt);
	
	DataObjectFunctionPtr ExecStatementFunctionDefinition(const StatementFunctionDefinitionPtr& stmt);

	void ExecStatement(const StatementPtr& stmt, VoidPtr& func_result);

	void ExecStatementBlock(const StatementBlockPtr& block, VoidPtr& func_result);

	void ExecStatementIf(const StatementIfPtr& stmt, VoidPtr& func_result);

	void ExecStatementFor(const StatementForPtr& stmt, VoidPtr& func_result);

	VoidPtr ExecFunctionCall(const DataObjectFunctionPtr& func_ptr, const VoidContainer& arguments);

	// return VoidPtr ?
	void ExecStatementTable(const StatementTablePtr& table);

	bool IsBranchMode() const;

	SuggestContainer* GetSuggestBranchVars() const;

	bool IsLocalVar(const BitPtr& var) const;

	SuggestContainer::const_iterator FindVarInSuggest(const BitPtr& var, const SuggestContainer& var_container) const;

	SuggestContainer::iterator FindVarInSuggest(const BitPtr& var, SuggestContainer& var_container) const;

private:

	void ExtractUniqueVars(const SuggestContainer& source, SuggestContainer& target) const;

private:

	//! Пропозициональная кодировка
	EncodeFactoryPtr encode_;

	//! Вычисляет выражения языка ТА
	IALUPtr alu_;
	
	struct SuggestBranchs
	{
		SuggestBranchs(): current_branch(0) {}
		LogicFormulaPtr condition;
		SuggestContainer suggest_true_branch;
		SuggestContainer suggest_false_branch;
		SuggestContainer *current_branch;
	};

	// класс, обеспечивающий пропозициональное кодирование условного перехода
	// поскольку допускаются вложенные операторы условного перехода, то BranchMatrix заносится в стек
	struct BranchMatrix
	{
		SuggestBranchs suggest_branchs;
		BitContainer local_vars;
	};

	typedef std::stack<BranchMatrix> BranchStack;
	typedef std::stack<DataObjectContextPtr> ContextStack;

	BranchStack branchs_;

	//! Стек контекстов
	//! В самом низу стека находится глобальный контекст
	ContextStack context_;
};

} // namespace Transalg
